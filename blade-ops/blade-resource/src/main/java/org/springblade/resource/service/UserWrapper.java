package org.springblade.resource.service;

import java.util.Optional;

import org.springblade.core.tool.api.R;
import org.springblade.resource.entity.VoBaseEntity;
import org.springblade.system.user.entity.User;
import org.springblade.system.user.feign.IUserClient;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@AllArgsConstructor
@Slf4j
public class UserWrapper  {
	private final IUserClient userClient ;
	
	public VoBaseEntity entityVO(VoBaseEntity o) {
		
		
		R<User> r=userClient.userInfoById(o.getCreateUser());
		log.info("{}",r);
		Optional.ofNullable(r.getData()).ifPresent(u ->o.setCreateUserName(u.getAccount()));
		return o;
	}
}
