/*
 *      Copyright (c) 2018-2028, Chill Zhuang All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the dreamlu.net developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: Chill 庄骞 (smallchill@163.com)
 */
package org.springblade.resource.service.impl;

import org.springblade.resource.entity.AttachLogs;
import org.springblade.resource.vo.AttachLogsVO;
import org.springblade.resource.mapper.AttachLogsMapper;
import org.springblade.resource.service.IAttachLogsService;
import org.springblade.core.mp.base.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * 文件日志表 服务实现类
 *
 * @author BladeX
 * @since 2021-07-23
 */
@Service
public class AttachLogsServiceImpl extends BaseServiceImpl<AttachLogsMapper, AttachLogs> implements IAttachLogsService {
    
	@Autowired
	private  AttachLogsMapper  attachLogsMapper;
	@Override
	public IPage<AttachLogsVO> selectAttachLogsPage(IPage<AttachLogsVO> page, AttachLogsVO attachLogs) {
		return page.setRecords(baseMapper.selectAttachLogsPage(page, attachLogs));
	}
    
	public void logs(Long id ,int rate ,String log) {
		attachLogsMapper.updateRate( id , rate , log);
	}
}
