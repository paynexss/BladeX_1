/*
 *      Copyright (c) 2018-2028, Chill Zhuang All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the dreamlu.net developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: Chill 庄骞 (smallchill@163.com)
 */
package org.springblade.resource.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import org.springblade.core.mp.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 附件表实体类
 *
 * @author BladeX
 * @since 2021-07-22
 */
@Data
@TableName("blade_annexe")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "Annexe对象", description = "附件表")
public class Annexe extends BaseEntity {

	private static final long serialVersionUID = 1L;

	/**
	 * 业务类型
	 */
	@ApiModelProperty(value = "业务类型")
	private String bizType;
	/**
	 * 业务表ID
	 */
	@ApiModelProperty(value = "业务表ID")
	private Long bizId;
	/**
	 * 原始文件名称
	 */
	@ApiModelProperty(value = "原始文件名称")
	private String fileName;
	/**
	 * 文件存放路径
	 */
	@ApiModelProperty(value = "文件存放路径")
	private String fileStoragePath;
	/**
	 * fastDFS-权限认证token
	 */
	@ApiModelProperty(value = "fastDFS-权限认证token")
	private String fileStorageToken;
	/**
	 * 文件类型
	 */
	@ApiModelProperty(value = "文件类型")
	@TableField("File_type")
	private String fileType;
	/**
	 * 文件大小
	 */
	@ApiModelProperty(value = "文件大小")
	@TableField("File_size")
	private Long fileSize;

	/**
	 * 下载文件路径
	 */
	@ApiModelProperty(value = "下载文件路径")
	@TableField(exist = false)
	private String filePath;

	public Annexe(){}

	public Annexe(String bizType,Long bizId,String fileName,String fileStoragePath,String fileStorageToken,String fileType,Long fileSize,String filePath){
		this.bizType = bizType;
		this.bizId = bizId;
		this.fileName = fileName;
		this.fileStoragePath = fileStoragePath;
		this.fileStorageToken = fileStorageToken;
		this.fileType = fileType;
		this.fileSize = fileSize;
		this.filePath = filePath;
	}


}
