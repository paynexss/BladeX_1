/*
 *      Copyright (c) 2018-2028, Chill Zhuang All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the dreamlu.net developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: Chill 庄骞 (smallchill@163.com)
 */
package org.springblade.resource.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import org.springblade.core.mp.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 文件日志表实体类
 *
 * @author BladeX
 * @since 2021-07-23
 */
@Data
@TableName("blade_attach_logs")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "AttachLogs对象", description = "文件日志表")
public class AttachLogs extends VoBaseEntity {

	private static final long serialVersionUID = 1L;

	/**
	* 文件名称
	*/
		@ApiModelProperty(value = "文件名称")
		private String name;
	/**
	* 文件ID
	*/
		@ApiModelProperty(value = "文件ID")
		private Long fileId;
	/**
	* 日志
	*/
		@ApiModelProperty(value = "日志")
		private String logs;
	/**
	* 进度
	*/
		@ApiModelProperty(value = "进度")
		private Integer rateof;


}
