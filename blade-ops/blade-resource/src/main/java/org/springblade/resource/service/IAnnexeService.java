/*
 *      Copyright (c) 2018-2028, Chill Zhuang All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the dreamlu.net developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: Chill 庄骞 (smallchill@163.com)
 */
package org.springblade.resource.service;

import org.springblade.resource.client.FastDFSException;
import org.springblade.resource.entity.Annexe;
import org.springblade.resource.vo.AnnexeVO;
import org.springblade.core.mp.base.BaseService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * 附件表 服务类
 *
 * @author BladeX
 * @since 2021-07-22
 */
public interface IAnnexeService extends BaseService<Annexe> {

	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param annexe
	 * @return
	 */
	IPage<AnnexeVO> selectAnnexePage(IPage<AnnexeVO> page, AnnexeVO annexe);


	List<Annexe> findList(Annexe annexe);

	boolean batchSave(String bizType, Long bizId, MultipartFile[] files) throws IOException, FastDFSException;

	/**
	 * 保存文件到服务器并返回服务器地址
	 *
	 * @param bizType
	 * @param bizId
	 * @param file
	 * @return
	 * @throws IOException
	 */
	String saveForUrl(String bizType, Long bizId, MultipartFile file) throws IOException;

	boolean remove(List<Long> idList);

	boolean copyAndOverwriteUpdate(String fromBizType, Long fromBizId, String toBizType, Long toBizId, boolean overwriteUpdate);

	/**
	 * 根据业务类型和业务id删除记录
	 *
	 * @param bizType 业务类型
	 * @param bizId   业务id
	 * @return
	 */
	boolean removeByBizIdAndType(String bizType, Long bizId);
}
