package org.springblade.resource.entity;

import java.beans.Transient;

import org.springblade.core.mp.base.BaseEntity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class VoBaseEntity extends BaseEntity {
	@ApiModelProperty(value = "创建人")
	private transient String createUserName;
}
