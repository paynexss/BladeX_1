package org.springblade.resource.service.impl;

import java.time.Instant;import java.time.LocalDateTime;
import java.util.Date;

import org.springblade.resource.entity.AttachFile;
import org.springblade.resource.entity.AttachLogs;
import org.springblade.resource.service.IAttachFileService;
import org.springblade.resource.service.IAttachLogsService;
import org.springblade.resource.service.PutinService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component("putinService")
@Slf4j
public class TestPutinService implements PutinService {
	
	@Autowired
	private IAttachLogsService   attachLogsService;
	@Autowired
	private   IAttachFileService  attachFileService;
	@Async("taskExecutor")
	@Override
	public void doSomething(AttachFile file, AttachLogs log1) {
		
		long start  = Instant.now().toEpochMilli();
		log.info("file is : {} ; start time  is : {}" ,file.getName() ,start);
		
		try {
			attachLogsService.logs(log1.getId(), 100, "完成入库！");
			
			log1  = attachLogsService.getById(log1.getId());
			log1.setStatus(org.springblade.resource.util.Status.Success.ordinal());
			attachLogsService.updateById(log1);
			
			file.setPutStatus("已入库");
			attachFileService.updateById(file);
		} catch (Exception e) {
			e.printStackTrace();
			log1  = attachLogsService.getById(log1.getId());
			log1.setUpdateTime(new Date());
			log1.setStatus(org.springblade.resource.util.Status.Fail.ordinal());
			attachLogsService.updateById(log1);
			
			file.setPutStatus("入库失败");
			attachFileService.updateById(file);
		} finally {
			// TODO: handle finally clause
		}
		
		long end   = Instant.now().toEpochMilli();
		
		log.info("file is : {} ; start time  is : {}  . time cost : {}" ,file.getName() ,end , end -start);

	}

}
