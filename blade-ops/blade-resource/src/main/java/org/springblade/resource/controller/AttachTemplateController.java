/*
 *      Copyright (c) 2018-2028, Chill Zhuang All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the dreamlu.net developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: Chill 庄骞 (smallchill@163.com)
 */
package org.springblade.resource.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.google.gson.Gson;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springblade.core.mp.support.Condition;
import org.springblade.core.mp.support.Query;
import org.springblade.core.tool.api.R;
import org.springblade.core.tool.api.ResultCode;
import org.springblade.core.tool.utils.Func;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springblade.resource.entity.AttachTemplate;
import org.springblade.resource.hadoop.HadoopProperties;
import org.springblade.resource.vo.AttachTemplateVO;
import org.springblade.resource.service.FileBridgeService;
import org.springblade.resource.service.IAttachTemplateService;
import org.springblade.resource.service.UserWrapper;
import org.springblade.resource.util.FileUtil;
import org.springblade.core.boot.ctrl.BladeController;

/**
 * 附件模板表 控制器
 *
 * @author BladeX
 * @since 2021-07-21
 */
@RestController
@AllArgsConstructor
@Slf4j
@RequestMapping("/attachtemplate")
@Api(value = "附件模板表", tags = "附件模板表接口")
public class AttachTemplateController extends BladeController {

	private final IAttachTemplateService attachTemplateService;
	private  final  FileBridgeService  fileBridgeService;
    private final  UserWrapper  userWrapper;
	/**
	 * 详情
	 */
	@GetMapping("/detail")
	@ApiOperationSupport(order = 1)
	@ApiOperation(value = "详情", notes = "传入attachTemplate")
	public R<AttachTemplate> detail(AttachTemplate attachTemplate) {
		AttachTemplate detail = attachTemplateService.getOne(Condition.getQueryWrapper(attachTemplate));
		userWrapper.entityVO(detail);
		return R.data(detail);
	}

	/**
	 * 分页 附件模板表
	 */
	@GetMapping("/list")
	@ApiOperationSupport(order = 2)
	@ApiOperation(value = "分页", notes = "传入attachTemplate")
	public R<IPage<AttachTemplate>> list(AttachTemplate attachTemplate, Query query) {
		IPage<AttachTemplate> pages = attachTemplateService.page(Condition.getPage(query), Condition.getQueryWrapper(attachTemplate));
		pages.getRecords().forEach(vo ->{
			userWrapper.entityVO(vo);
		});
		return R.data(pages);
	}

	/**
	 * 自定义分页 附件模板表
	 */
	@GetMapping("/page")
	@ApiOperationSupport(order = 3)
	@ApiOperation(value = "分页", notes = "传入attachTemplate")
	public R<IPage<AttachTemplateVO>> page(AttachTemplateVO attachTemplate, Query query) {
		IPage<AttachTemplateVO> pages = attachTemplateService.selectAttachTemplatePage(Condition.getPage(query), attachTemplate);
		pages.getRecords().forEach(vo ->{
			userWrapper.entityVO(vo);
		});
		return R.data(pages);
	}

	/**
	 * 新增 附件模板表
	 */
	@PostMapping("/save")
	@ApiOperationSupport(order = 4)
	@ApiOperation(value = "新增", notes = "传入attachTemplate")
	public R save(@Valid @RequestBody AttachTemplate attachTemplate) {
		return R.status(attachTemplateService.save(attachTemplate));
	}
	
	private HadoopProperties hadoopProperties;
	/**
	 * 新增 附件模板表
	 */
	@PostMapping("/savewith")
	@ApiOperationSupport(order = 4)
	@ApiOperation(value = "新增 file", notes = "传入attachTemplate ,file")
	public R savewith(@Valid @RequestParam String attach,MultipartFile file) {
		
		AttachTemplate  attachTemplate = new Gson().fromJson(attach, AttachTemplate.class);
		if(file !=null) {
			log.info("file size is {}",file.getSize());
			if(file.getSize()>hadoopProperties.getMax()) {
				return R.fail(ResultCode.METHOD_NOT_SUPPORTED,"the file is too large!");
			}
			if(FileUtil.isNoRule(file.getOriginalFilename())) {
				return R.fail(ResultCode.METHOD_NOT_SUPPORTED,"the file name is wrong!");
			}
		}
		
		attachTemplateService.save(attachTemplate);
		
		if(file!=null) {
			String uploadPath = fileBridgeService.filePath(attachTemplate.getId(),file.getOriginalFilename());
			fileBridgeService.copyLocalToDistributed(FileUtil.MultipartFileToFile(file).getPath(), uploadPath, true, true);
			
			attachTemplate.setLink(uploadPath);
			attachTemplate.setOriginalName(file.getOriginalFilename());
			attachTemplateService.updateById(attachTemplate);
		}
		
		return R.data(attachTemplate);
	}

	/**
	 * 修改 附件模板表
	 */
	@PostMapping("/update")
	@ApiOperationSupport(order = 5)
	@ApiOperation(value = "修改", notes = "传入attachTemplate")
	public R update(@Valid @RequestBody AttachTemplate attachTemplate) {
		return R.status(attachTemplateService.updateById(attachTemplate));
	}
    
	/**
	 * 修改 附件模板表
	 */
	@PostMapping("/updatewith")
	@ApiOperationSupport(order = 5)
	@ApiOperation(value = "修改 file", notes = "传入attachTemplate,file")
	public R update(@Valid @RequestParam String attach,MultipartFile file) {
		
		AttachTemplate  attachTemplate = new Gson().fromJson(attach, AttachTemplate.class);
		if(attachTemplate.getId() == null) {
			return R.fail(ResultCode.METHOD_NOT_SUPPORTED,"xxxxxxx!");
		}
		if(file !=null) {
			log.info("file size is {}",file.getSize());
			if(file.getSize()>hadoopProperties.getMax()) {
				return R.fail(ResultCode.METHOD_NOT_SUPPORTED,"the file is too large!");
			}
			if(FileUtil.isNoRule(file.getOriginalFilename())) {
				return R.fail(ResultCode.METHOD_NOT_SUPPORTED,"the file name is wrong!");
			}
			String uploadPath = fileBridgeService.filePath(attachTemplate.getId(),file.getOriginalFilename());
			fileBridgeService.copyLocalToDistributed(FileUtil.MultipartFileToFile(file).getPath(), uploadPath, true, true);
			attachTemplate.setLink(uploadPath);
			attachTemplate.setOriginalName(file.getOriginalFilename());
		}
		attachTemplateService.updateById(attachTemplate);
		return R.data(attachTemplate);
	}
	
	/**
	 * 新增或修改 附件模板表
	 */
	@PostMapping("/submit")
	@ApiOperationSupport(order = 6)
	@ApiOperation(value = "新增或修改", notes = "传入attachTemplate")
	public R submit(@Valid @RequestBody AttachTemplate attachTemplate) {
		return R.status(attachTemplateService.saveOrUpdate(attachTemplate));
	}

	
	/**
	 * 删除 附件模板表
	 */
	@PostMapping("/remove")
	@ApiOperationSupport(order = 7)
	@ApiOperation(value = "逻辑删除", notes = "传入ids")
	public R remove(@ApiParam(value = "主键集合", required = true) @RequestParam String ids) {
		return R.status(attachTemplateService.deleteLogic(Func.toLongList(ids)));
	}
    
	 /**
     * 下载文件
     */
    @GetMapping("download")
    public void download(@RequestParam Long id, HttpServletResponse response) {
    	AttachTemplate  t = attachTemplateService.getById(id);
    	if(t == null) {
    		return;
    	}
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Expires", "0");
        response.setHeader("charset", "utf-8");
        response.setContentType("application/force-download");
        response.setHeader("Content-Transfer-Encoding", "binary");
        response.setHeader("Content-Disposition", "attachment;filename=\"" + t.getOriginalName() + "\"");
        OutputStream os;
        try {
            os = response.getOutputStream();
            fileBridgeService.download(t.getLink(), os);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
