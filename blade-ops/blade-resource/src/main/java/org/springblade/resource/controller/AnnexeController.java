/*
 *      Copyright (c) 2018-2028, Chill Zhuang All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the dreamlu.net developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: Chill 庄骞 (smallchill@163.com)
 */
package org.springblade.resource.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springblade.core.boot.ctrl.BladeController;
import org.springblade.core.mp.support.Condition;
import org.springblade.core.mp.support.Query;
import org.springblade.core.tool.api.R;
import org.springblade.core.tool.utils.Func;
import org.springblade.resource.entity.Annexe;
import org.springblade.resource.service.IAnnexeService;
import org.springblade.resource.vo.AnnexeVO;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 附件表 控制器
 *
 * @author BladeX
 * @since 2021-07-22
 */
@RestController
@AllArgsConstructor
@RequestMapping("/annexe")
@Api(value = "附件表", tags = "附件表接口")
public class AnnexeController extends BladeController {

	private final IAnnexeService annexeService;

	/**
	 * 详情
	 */
	@GetMapping("/detail")
	@ApiOperationSupport(order = 1)
	@ApiOperation(value = "详情")
	public R<Annexe> detail(Annexe annexe) {
		Annexe detail = annexeService.getOne(Condition.getQueryWrapper(annexe));
		return R.data(detail);
	}


	@GetMapping("/list")
	@ApiOperationSupport(order = 2)
	@ApiOperation(value = "列表")
	public R<List<Annexe>> list(Annexe annexe) {
		List<Annexe> dataList = annexeService.findList(annexe);
		return R.data(dataList);
	}

	/**
	 * 自定义分页 附件表
	 */
	@GetMapping("/page")
	@ApiOperationSupport(order = 3)
	@ApiOperation(value = "分页")
	public R<IPage<AnnexeVO>> page(AnnexeVO annexe, Query query) {
		IPage<AnnexeVO> pages = annexeService.selectAnnexePage(Condition.getPage(query), annexe);
		return R.data(pages);
	}

	/**
	 * 新增 附件表
	 */
	@PostMapping("/save")
	@ApiOperationSupport(order = 4)
	@ApiOperation(value = "新增")
	public R save(@Valid @RequestBody Annexe annexe) {
		return R.status(annexeService.save(annexe));
	}

	/**
	 * 修改 附件表
	 */
	@PostMapping("/update")
	@ApiOperationSupport(order = 5)
	@ApiOperation(value = "修改")
	public R update(@Valid @RequestBody Annexe annexe) {
		return R.status(annexeService.updateById(annexe));
	}



	/**
	 * 删除 附件表
	 */
	@PostMapping("/remove")
	@ApiOperationSupport(order = 7)
	@ApiOperation(value = "逻辑删除")
	public R remove(@ApiParam(value = "主键集合", required = true) @RequestParam String ids) {
		return R.status(annexeService.deleteLogic(Func.toLongList(ids)));
	}


}
