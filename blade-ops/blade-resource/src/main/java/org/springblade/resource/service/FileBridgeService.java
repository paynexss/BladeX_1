package org.springblade.resource.service;

import java.io.IOException;
import java.io.OutputStream;

public interface FileBridgeService {
	
	public String filePath(String fileName);
	public String filePath(Long id,String fileName);
	
	public boolean copyLocalToDistributed(String srcFile, String dstHdfsPath, boolean deleteSource, boolean overwrite);
	public boolean copyDistributedToLocal(String srcHdfsFilePath, String dstFile, boolean deleteSource, boolean overwrite);
	
	public void download(String path, String fileName, OutputStream outputStream)  throws IOException;
	public void download(String path,  OutputStream outputStream)  throws IOException;
}
