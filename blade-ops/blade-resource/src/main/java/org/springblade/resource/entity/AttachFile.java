/*
 *      Copyright (c) 2018-2028, Chill Zhuang All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the dreamlu.net developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: Chill 庄骞 (smallchill@163.com)
 */
package org.springblade.resource.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import org.springblade.core.mp.base.BaseEntity;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 文件表实体类
 *
 * @author BladeX
 * @since 2021-07-23
 */
@Data
@TableName("blade_attach_file")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "AttachFile对象", description = "文件表")
public class AttachFile extends VoBaseEntity {

	private static final long serialVersionUID = 1L;

	/**
	* 文件地址
	*/
		@ApiModelProperty(value = "文件地址")
		private String link;
	/**
	* 文件域名
	*/
		@ApiModelProperty(value = "文件域名")
		private String domaina;
	/**
	* 文件名称
	*/
		@ApiModelProperty(value = "文件名称")
		private String name;
	/**
	* 文件原名
	*/
		@ApiModelProperty(value = "文件原名")
		private String originalName;
	/**
	* 文件拓展名
	*/
		@ApiModelProperty(value = "文件拓展名")
		private String extension;
	/**
	* 文件大小
	*/
		@ApiModelProperty(value = "文件大小")
		private Long attachSize;
	/**
	* 元数据id
	*/
		@ApiModelProperty(value = "元数据id")
		private String metaId;
	/**
	* 元数据名称
	*/
		@ApiModelProperty(value = "元数据名称")
		private String metaName;
	/**
	* 文件类型
	*/
		@ApiModelProperty(value = "文件类型")
		private String fileType;
	/**
	* 入库状态
	*/
		@ApiModelProperty(value = "入库状态")
		private String putStatus;
	/**
	* 提供部门
	*/
		@ApiModelProperty(value = "提供部门")
		private String dep;
	/**
	* 关键字
	*/
		@ApiModelProperty(value = "关键字")
		private String keyss;
	/**
	* 数据领域
	*/
		@ApiModelProperty(value = "数据领域")
		private String dataDomain;
	/**
	* 所属行业
	*/
		@ApiModelProperty(value = "所属行业")
		private String business;
	/**
	* 汇聚时间
	*/
		@ApiModelProperty(value = "汇聚时间")
		private LocalDateTime putTime;
	/**
	* 模板id
	*/
		@ApiModelProperty(value = "模板id")
		private Long templateId;


}
