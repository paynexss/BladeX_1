package org.springblade.resource.service.impl;

import java.io.IOException;
import java.io.OutputStream;
import java.security.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;

import org.springblade.resource.hadoop.HadoopClient;
import org.springblade.resource.hadoop.HadoopProperties;
import org.springblade.resource.service.FileBridgeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component(value = "fileBridgeService")
public class HadoopBridgeService implements FileBridgeService {
	
	@Autowired
	private  HadoopClient  hadoopClient;
	
	@Autowired
	private  HadoopProperties  hadoopProperties;
	@Override
	public boolean copyLocalToDistributed(String srcFile, String dstHdfsPath, boolean deleteSource, boolean overwrite) {
		hadoopClient.copyFileToHDFS(deleteSource, overwrite, srcFile, dstHdfsPath);
		return true;
	}

	@Override
	public boolean copyDistributedToLocal(String srcHdfsFilePath, String dstFile, boolean deleteSource,
			boolean overwrite) {
		hadoopClient.downloadFileFromLocal(srcHdfsFilePath, dstFile);
		return true;
	}

	@Override
	public void download(String path, String fileName, OutputStream outputStream) throws IOException {
		hadoopClient.download(path, fileName, outputStream);
	}

	@Override
	public String filePath(String fileName) {
		return hadoopProperties.getDirectoryPath()+Instant.now().toEpochMilli()+"_"+fileName;
	}

	@Override
	public void download(String path, OutputStream outputStream) throws IOException {
		// TODO Auto-generated method stub
		hadoopClient.download(path, outputStream);

	}

	@Override
	public String filePath(Long id, String fileName) {
		// TODO Auto-generated method stub
		return hadoopProperties.getDirectoryPath()+id+"_"+fileName;
	}

}
