/*
 *      Copyright (c) 2018-2028, Chill Zhuang All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the dreamlu.net developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: Chill 庄骞 (smallchill@163.com)
 */
package org.springblade.flow.engine.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springblade.core.mp.support.Condition;
import org.springblade.core.mp.support.Query;
import org.springblade.core.secure.BladeUser;
import org.springblade.core.secure.annotation.PreAuth;
import org.springblade.core.tenant.annotation.NonDS;
import org.springblade.core.tool.api.R;
import org.springblade.core.tool.constant.RoleConstant;
import org.springblade.flow.engine.entity.ActivitiModelEntity;
import org.springblade.flow.engine.service.ActivitiModelService;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.Date;
import java.util.Map;

/**
 * 流程模型控制器
 *
 * @author Chill
 */
@NonDS
@RestController
@RequestMapping("/activitiModel")
@AllArgsConstructor
@PreAuth(RoleConstant.HAS_ROLE_ADMINISTRATOR)
public class ActivitiModelController {

	private final ActivitiModelService activitiModelService;

	/**
	 * 分页
	 */
	@GetMapping("/list")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "modelKey", value = "模型标识", paramType = "query", dataType = "string"),
		@ApiImplicitParam(name = "name", value = "模型名称", paramType = "query", dataType = "string")
	})
	@ApiOperationSupport(order = 1)
	@ApiOperation(value = "分页", notes = "传入notice")
	public R<IPage<ActivitiModelEntity>> list(@ApiIgnore @RequestParam Map<String, Object> activitiModelEntity, Query query) {
		IPage<ActivitiModelEntity> pages = activitiModelService.page(Condition.getPage(query), Condition.getQueryWrapper(activitiModelEntity, ActivitiModelEntity.class));
		return R.data(pages);
	}


	/**
	 * 新增或修改
	 */
	/*@PostMapping("/save")
	@ApiOperationSupport(order = 3)
	@ApiOperation(value = "新增", notes = "传入activitiModel")
	public R save(@Valid @RequestBody ActivitiModelEntity activitiModel, BladeUser bladeUser) {

		String tenantsId = bladeUser.getTenantId();
		String authorization = request.getHeader("Authorization");
		if(StringUtils.isNotBlank(tenantsId)) {
			activitiModel.setTenantsId(Long.valueOf(tenantsId));
		}
		activitiModel.setCreateBy(bladeUser.getUserName());
		activitiModel.setStatus("1");
		activitiModel.setCreateDate(new Date());
		String modelId = activitiModelService.newMyModel(authorization, activitiModel);
		activitiModel.setModelId(modelId);
		//activitiModelService.save(activitiModel);

		return R.status(activitiModelService.saveOrUpdate(activitiModel));
	}*/
}
