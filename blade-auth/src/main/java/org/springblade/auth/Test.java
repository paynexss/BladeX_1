package org.springblade.auth;

import com.alibaba.fastjson.JSONObject;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.jwt.crypto.sign.MacSigner;
import org.springframework.security.oauth2.common.DefaultExpiringOAuth2RefreshToken;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.DefaultOAuth2RefreshToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.common.util.RandomValueStringGenerator;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

public class Test {
	public static void test(String[] args){
		//JwtAccessTokenConverter converter =new JwtAccessTokenConverter();
		DefaultOAuth2AccessToken result = new DefaultOAuth2AccessToken("222");
		Map<String, Object> info = new LinkedHashMap();
		info.put("aaaaaaaaaaaaaaaaa","fffffffffffffffffffffffffffffffff");
		result.setAdditionalInformation(info);
		String verifierKey = (new RandomValueStringGenerator()).generate();
		JSONObject data=new JSONObject();
		data.put("id","8");
		data.put("time",10);
		result.setValue(JwtHelper.encode(data.toJSONString(), new MacSigner(verifierKey)).getEncoded());
		System.out.println(result.getValue());
		DefaultOAuth2RefreshToken token=new DefaultExpiringOAuth2RefreshToken(result.getValue(), new Date());
		result.setRefreshToken((OAuth2RefreshToken)token);
		System.out.println(result);
		System.out.println(result.getAdditionalInformation());
		Jwt jwt = JwtHelper.decode(result.getValue() );
		System.out.println(jwt.getEncoded());
		System.out.println(jwt.getClaims());

	}
}
