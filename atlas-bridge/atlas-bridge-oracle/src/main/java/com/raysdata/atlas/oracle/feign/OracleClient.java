/*
 *      Copyright (c) 2018-2028, Chill Zhuang All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the dreamlu.net developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: Chill 庄骞 (smallchill@163.com)
 */
package com.raysdata.atlas.oracle.feign;


import com.raysdata.atlas.oracle.service.MetadataService;
import lombok.AllArgsConstructor;
import org.springblade.core.tenant.annotation.NonDS;
import org.springblade.core.tool.api.R;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;



/**
 * 字典服务Feign实现类
 *
 * @author Chill
 */
@NonDS
@RestController
@AllArgsConstructor
public class OracleClient implements IOracleClient {

	private final MetadataService service;

	@Override
	@GetMapping(GET_TABELS)
	public R<List<String>> getTables(String dataSourceId) {
		return R.data(service.getTables(dataSourceId));
	}

	@Override
	@GetMapping(GET_COLUMNS)
	public R<List<String>> getColumns(String dataSourceId,String tableName) {
		return R.data(service.getColumns(dataSourceId,tableName));
	}

}
