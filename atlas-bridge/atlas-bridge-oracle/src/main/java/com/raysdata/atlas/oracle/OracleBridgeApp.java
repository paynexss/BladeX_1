package com.raysdata.atlas.oracle;

import org.springblade.core.cloud.feign.EnableBladeFeign;
import org.springblade.core.launch.BladeApplication;
import org.springframework.cloud.client.SpringCloudApplication;

@EnableBladeFeign
@SpringCloudApplication
public class OracleBridgeApp {

	public static void main(String[] args) {

		BladeApplication.run("oracleBridge", OracleBridgeApp.class, args);
	}

}
