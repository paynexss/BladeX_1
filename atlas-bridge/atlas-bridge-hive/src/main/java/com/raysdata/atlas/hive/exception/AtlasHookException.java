package com.raysdata.atlas.hive.exception;

/**
 * Exception class for Atlas Hooks.
 */
public class AtlasHookException extends Exception {

    public AtlasHookException() {
    }

    public AtlasHookException(String message) {
        super(message);
    }

    public AtlasHookException(String message, Throwable cause) {
        super(message, cause);
    }

    public AtlasHookException(Throwable cause) {
        super(cause);
    }

    public AtlasHookException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
