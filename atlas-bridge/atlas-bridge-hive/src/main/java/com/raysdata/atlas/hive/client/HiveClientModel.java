package com.raysdata.atlas.hive.client;

import lombok.Data;

@Data
public class HiveClientModel {
	
	/**
	 * thrift://10.91.64.23:9083,thrift://10.91.64.23:9083,thrift://10.91.64.23:9083
	 */
	private String hive_metastore_uris;
	
	private String metadataNamespace;
	private boolean       deleteNonExisting =false;
	private boolean       failOnError=false;
	private String        databaseToImport;
	private String        tableToImport;
	
	private String address;
	private String jdbcUrl;
	private String user;
	private String password;
}
