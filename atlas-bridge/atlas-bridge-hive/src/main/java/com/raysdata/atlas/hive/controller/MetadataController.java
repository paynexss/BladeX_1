package com.raysdata.atlas.hive.controller;

import lombok.extern.slf4j.Slf4j;

import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.springblade.core.tool.api.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.raysdata.atlas.common.DataSource;
import com.raysdata.atlas.common.MetadataService;
import com.raysdata.atlas.hive.client.AtlasClientService;
import com.raysdata.atlas.hive.client.HiveClientModel;

import java.util.List;

@RestController
@RequestMapping("/metadata")
@Slf4j
public class MetadataController {

	@Autowired
	private AtlasClientService atlasClientService;

	@GetMapping("/ingest")
	public String getTables(@RequestParam String datasourceId) {
		String result = "ok";
		
		try {
			
			DataSource  d = metadataService.getDataSource(datasourceId);
			HiveClientModel model = new HiveClientModel();
			model.setDatabaseToImport(d.getDatabase());
			model.setHive_metastore_uris(d.getJdbcUrl());
			model.setMetadataNamespace("cdh-test");
			atlasClientService.syn(model);
		} catch (HiveException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = e.getMessage();
		}
		return result;
	}

	@Autowired
	private MetadataService metadataService;
    
	@GetMapping("/test")
	public DataSource getDatasource(@RequestParam String datasourceId) {
		return metadataService.getDataSource(datasourceId);
	}

}
