package com.raysdata.atlas;

import org.springblade.core.cloud.feign.EnableBladeFeign;
import org.springblade.core.launch.BladeApplication;
import org.springframework.cloud.client.SpringCloudApplication;


@EnableBladeFeign
@SpringCloudApplication
public class App {

	public static void main(String[] args) {
		BladeApplication.run("hive_1_1_1_Bridge", App.class, args);
	}

}