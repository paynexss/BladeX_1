package com.raysdata.atlas.common;

import java.util.HashMap;
import java.util.Map;

import org.apache.atlas.AtlasClientV2;
import org.apache.atlas.AtlasServiceException;
import org.apache.atlas.model.discovery.AtlasSearchResult;
import org.apache.atlas.model.instance.AtlasEntity;
import org.apache.commons.lang3.StringUtils;

public class AtlasUtil {
	public static AtlasSearchResult  searchByQualifiedName(AtlasClientV2 atlasClientV2,String type,String qualifiedName) throws AtlasServiceException {
		AtlasSearchResult  result =  atlasClientV2.dslSearch(String.format("`%s` where qualifiedName=\"%s\"", type,qualifiedName));
		return result;
	}
	
	public static void addDateSourId(AtlasEntity entity , int id) {
		Map<String, String> cas = entity.getCustomAttributes();
		if(cas == null) {
			cas = new HashMap<>();
		}
		
		cas.put(SystemConstant.USER_DEFINED_DATASOURCEID, String.valueOf(id));
		
		entity.setCustomAttributes(cas);
	}
}
