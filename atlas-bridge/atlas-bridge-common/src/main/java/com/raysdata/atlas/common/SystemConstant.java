package com.raysdata.atlas.common;

public class SystemConstant {
    public static String USER_DEFINED_USERID="userid";
    public static String USER_DEFINED_DATASOURCEID="datasourceid";
    public static String USER_DEFINED_USERNAME="username";
    
    public static String USER_DEFINED_GRADE="grade";
    public static String USER_DEFINED_GRADE_ID="gradeid";
    
    
    public static String GLOSSARY_GTOUP_NAME = "gids";
}
