package com.raysdata.atlas.mysql.controller;

import com.raysdata.atlas.common.DataSource;
import com.raysdata.atlas.mysql.client.AtlasClientService;
import com.raysdata.atlas.mysql.client.MysqlClientModel;
import com.raysdata.atlas.mysql.service.MetadataService;
import lombok.extern.slf4j.Slf4j;
import org.springblade.metadata.feign.IMetadataVersionClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/metadata")
@Slf4j
public class MetadataController {

	@Autowired
	private AtlasClientService atlasClientService;
	@Autowired
	private MetadataService metadataService;
	@Autowired
	private IMetadataVersionClient iMetadataVersionClient;

	@GetMapping("/mysql")
	public String syn(@RequestParam String datasourceId) {
		try {
			DataSource dataSource = metadataService.getDataSource(datasourceId);
			dataSource.setId(Integer.parseInt(datasourceId));
			MysqlClientModel model = new MysqlClientModel();
			model.setUser(dataSource.getUserName());
			model.setPassword(dataSource.getPassword());
			model.setDatabaseToImport(dataSource.getDatabase());
			model.setJdbcUrl(dataSource.getJdbcUrl());
			atlasClientService.syn(model,iMetadataVersionClient,dataSource);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}


}
