package com.raysdata.atlas.mysql;

import org.springblade.core.cloud.feign.EnableBladeFeign;
import org.springblade.core.launch.BladeApplication;
import org.springframework.cloud.client.SpringCloudApplication;

@EnableBladeFeign
@SpringCloudApplication
public class MysqlBridgeApp {

	public static void main(String[] args) {

		BladeApplication.run("mysqlBridge", MysqlBridgeApp.class, args);
	}

}
