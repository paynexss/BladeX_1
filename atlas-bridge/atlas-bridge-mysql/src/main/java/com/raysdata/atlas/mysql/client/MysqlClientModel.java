package com.raysdata.atlas.mysql.client;

import lombok.Data;

/**
 * "database": "kettle_des", "password": "******", "address":
 * "jdbc:mysql://10.10.100.120:30006", "jdbcUrl":
 * "jdbc:mysql://10.10.100.120:30006/kettle_des", "user": "root"
 * 
 * @author wangshuo
 *
 */
@Data
public class MysqlClientModel {

	private String address;
	private String jdbcUrl;
	private String user;
	private String password;
	private String databaseToImport;
	private String tableToImport;

	private String metadataNamespace;
	private boolean deleteNonExisting = false;
	private boolean failOnError = false;
}
