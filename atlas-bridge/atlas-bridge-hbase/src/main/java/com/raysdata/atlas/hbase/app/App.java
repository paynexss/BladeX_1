package com.raysdata.atlas.hbase.app;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/")
@Slf4j
public class App {
	@RequestMapping("*")
    String index(HttpServletRequest request){
    	String path  = request.getContextPath();
    	log.info(path);
        return "index";
    }
}
