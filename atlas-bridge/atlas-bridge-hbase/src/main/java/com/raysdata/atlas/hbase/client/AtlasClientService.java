package com.raysdata.atlas.hbase.client;

import javax.annotation.PreDestroy;

import org.apache.atlas.AtlasClientV2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.raysdata.atlas.hbase.model.HbaseClientModel;

import lombok.Getter;

@Service
@Getter
public class AtlasClientService {

	private AtlasClientV2 atlasClientV2;
	private String[] urls;
	private String name = "admin";
	private String pass = "admin";

	@Autowired
	public AtlasClientService(@Value("${atlas.url}") String[] urls, @Value("${atlas.name}") String name,
			@Value("${atlas.name}") String pass) {

		this.urls = urls;
		this.name = name;
		this.pass = pass;
		atlasClientV2 = new AtlasClientV2(this.urls, new String[] { this.name, this.pass });
	}
	
	public void syn(HbaseClientModel model) {
		HBaseBridgeService service =  new HBaseBridgeService(atlasClientV2, model);
		try {
			service.buildHBaseBridgeService();
			service.syn();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@PreDestroy
	void destroy() {
		if (atlasClientV2 != null) {
			atlasClientV2.close();
		}
	}
}
