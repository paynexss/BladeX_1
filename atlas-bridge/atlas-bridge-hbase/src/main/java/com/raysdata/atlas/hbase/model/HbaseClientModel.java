package com.raysdata.atlas.hbase.model;

import org.apache.hadoop.hbase.HBaseConfiguration;

import lombok.Data;

@Data
public class HbaseClientModel {
	private String hbase_zookeeper_quorum;
	private String hbase_zookeeper_port="2181";
	private String hbase_zookeeper_znode="/habse";
	private long   hbase_client_keyvalue_maxsize=1572864000;
	
	private String namespace;
	private String table;
	private String metadataNamespace;
	
	private String termId;
	
	public  org.apache.hadoop.conf.Configuration buildConf(){
		org.apache.hadoop.conf.Configuration conf = HBaseConfiguration.create();
		conf.set("hbase.zookeeper.quorum", hbase_zookeeper_quorum);
		conf.set("hbase.zookeeper.port", hbase_zookeeper_port);
		conf.set("hbase.zookeeper.znode", hbase_zookeeper_port);
		conf.set("hbase.zookeeper.port", hbase_zookeeper_port);
		return conf;
	}
}
