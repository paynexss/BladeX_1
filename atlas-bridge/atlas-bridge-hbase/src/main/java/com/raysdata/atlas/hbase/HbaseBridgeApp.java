package com.raysdata.atlas.hbase;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HbaseBridgeApp {

	public static void main(String[] args) {
		SpringApplication.run(HbaseBridgeApp.class, args);
	}

}
