/*
 *      Copyright (c) 2018-2028, Chill Zhuang All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the dreamlu.net developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: Chill 庄骞 (smallchill@163.com)
 */
package com.raysdata.rdc.dataasset.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 实体类
 *
 * @author BladeX
 * @since 2021-05-10
 */
@Data
@TableName("da_fire_data_statistics_every_day")
@ApiModel(value = "FireDataStatisticsEveryDay对象", description = "FireDataStatisticsEveryDay对象")
public class FireDataStatisticsEveryDay implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	/**
	* 汇聚数据总量
	*/
		@ApiModelProperty(value = "汇聚数据总量")
		private Long totalRecord;
	/**
	* 警情总数
	*/
		@ApiModelProperty(value = "警情总数")
		private Integer totalAlarm;
	/**
	* 数据日期
	*/
		@ApiModelProperty(value = "数据日期")
		private LocalDateTime dataDate;


}
