package com.raysdata.rdc.dataasset.dto;

import com.raysdata.rdc.dataasset.entity.Label;
import com.raysdata.rdc.dataasset.vo.LabelVO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

@Data
public class LabelNode  implements Serializable {
	private static final long serialVersionUID = 1L;
	LabelVO label;
	List<LabelNode> children;
}
