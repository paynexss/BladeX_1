package com.raysdata.rdc.dataasset.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import springfox.documentation.annotations.ApiIgnore;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 实体类
 *
 * @author mengjingji
 * @since 2021-04-28
 */
@Data
@TableName("da_topic")
@ApiModel(value = "Topic对象", description = "Topic对象")
public class Topic implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(hidden = true)
	private Long id;
	@ApiModelProperty(value = "图标")
	private String icon;
	@ApiModelProperty(value = "主题名称")
	private String name;
	@ApiModelProperty(value = "主题描述")
	private String description;
	@ApiModelProperty(hidden = true)
	private Long createUser;
	@ApiModelProperty(hidden = true)
	private LocalDateTime createTime;
	@ApiModelProperty(hidden = true)
	private Long updateUser;
	@ApiModelProperty(hidden = true)
	private LocalDateTime updateTime;
	//@TableLogic
	@ApiModelProperty(hidden = true)
	private Integer isDeleted;


}
