package com.raysdata.rdc.dataasset.vo;

import com.raysdata.rdc.dataasset.entity.Topic;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;

/**
 * 视图实体类
 *
 * @author mengjingji
 * @since 2021-04-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "TopicVO对象", description = "TopicVO对象")
public class TopicVO extends Topic {
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "资源目录数量")
	Integer catalogCount;
	@ApiModelProperty(value = "信息项数量")
	Integer dataFieldCount;

}
