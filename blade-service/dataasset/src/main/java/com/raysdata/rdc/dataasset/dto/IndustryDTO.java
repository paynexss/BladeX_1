package com.raysdata.rdc.dataasset.dto;

import com.raysdata.rdc.dataasset.entity.Industry;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 数据传输对象实体类
 *
 * @author mengjingji
 * @since 2021-04-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class IndustryDTO extends Industry {
	private static final long serialVersionUID = 1L;

}
