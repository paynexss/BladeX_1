/*
 *      Copyright (c) 2018-2028, Chill Zhuang All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the dreamlu.net developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: Chill 庄骞 (smallchill@163.com)
 */
package com.raysdata.rdc.dataasset.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import lombok.AllArgsConstructor;
import javax.validation.Valid;

import org.springblade.core.mp.support.Condition;
import org.springblade.core.mp.support.Query;
import org.springblade.core.tool.api.R;
import org.springblade.core.tool.utils.Func;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.raysdata.rdc.dataasset.entity.FireDataStatistics;
import com.raysdata.rdc.dataasset.vo.FireDataStatisticsVO;
import com.raysdata.rdc.dataasset.service.IFireDataStatisticsService;
import org.springblade.core.boot.ctrl.BladeController;

/**
 *  控制器
 *
 * @author BladeX
 * @since 2021-05-10
 */
@RestController
@AllArgsConstructor
@RequestMapping("/firedatastatistics")
@Api(value = "", tags = "消防统计接口")
public class FireDataStatisticsController extends BladeController {

	private final IFireDataStatisticsService fireDataStatisticsService;

	/**
	 * 详情
	 */
	@GetMapping("/detail")
	@ApiOperationSupport(order = 1)
	@ApiOperation(value = "详情", notes = "传入fireDataStatistics")
	public R<FireDataStatistics> detail(FireDataStatistics fireDataStatistics) {
		FireDataStatistics detail = fireDataStatisticsService.getOne(Condition.getQueryWrapper(fireDataStatistics));
		return R.data(detail);
	}

	/**
	 * 分页 
	 */
	@GetMapping("/list")
	@ApiOperationSupport(order = 2)
	@ApiOperation(value = "列表", notes = "传入fireDataStatistics")
	public R<IPage<FireDataStatistics>> list(FireDataStatistics fireDataStatistics, Query query) {
		IPage<FireDataStatistics> pages = fireDataStatisticsService.page(Condition.getPage(query), Condition.getQueryWrapper(fireDataStatistics));
		return R.data(pages);
	}

	/**
	 * 自定义分页 
	 */
	@GetMapping("/page")
	@ApiOperationSupport(order = 3)
	@ApiOperation(value = "分页", notes = "传入fireDataStatistics")
	public R<IPage<FireDataStatisticsVO>> page(FireDataStatisticsVO fireDataStatistics, Query query) {
		IPage<FireDataStatisticsVO> pages = fireDataStatisticsService.selectFireDataStatisticsPage(Condition.getPage(query), fireDataStatistics);
		return R.data(pages);
	}

	/**
	 * 新增 
	 */
	@PostMapping("/save")
	@ApiOperationSupport(order = 4)
	@ApiOperation(value = "新增", notes = "传入fireDataStatistics")
	public R save(@Valid @RequestBody FireDataStatistics fireDataStatistics) {
		return R.status(fireDataStatisticsService.save(fireDataStatistics));
	}

	/**
	 * 修改 
	 */
	@PostMapping("/update")
	@ApiOperationSupport(order = 5)
	@ApiOperation(value = "修改", notes = "传入fireDataStatistics")
	public R update(@Valid @RequestBody FireDataStatistics fireDataStatistics) {
		return R.status(fireDataStatisticsService.updateById(fireDataStatistics));
	}

	/**
	 * 新增或修改 
	 */
	@PostMapping("/submit")
	@ApiOperationSupport(order = 6)
	@ApiOperation(value = "新增或修改", notes = "传入fireDataStatistics")
	public R submit(@Valid @RequestBody FireDataStatistics fireDataStatistics) {
		return R.status(fireDataStatisticsService.saveOrUpdate(fireDataStatistics));
	}

	
	/**
	 * 删除 
	 */
	@PostMapping("/remove")
	@ApiOperationSupport(order = 8)
	@ApiOperation(value = "删除", notes = "传入ids")
	public R remove(@ApiParam(value = "主键集合", required = true) @RequestParam String ids) {
		return R.status(fireDataStatisticsService.removeByIds(Func.toLongList(ids)));
	}

	
}
