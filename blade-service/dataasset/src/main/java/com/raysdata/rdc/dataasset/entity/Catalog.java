/*
 *      Copyright (c) 2018-2028, Chill Zhuang All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the dreamlu.net developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: Chill 庄骞 (smallchill@163.com)
 */
package com.raysdata.rdc.dataasset.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 实体类
 *
 * @author BladeX
 * @since 2021-04-29
 */
@Data
@TableName("da_catalog")
@ApiModel(value = "Catalog对象", description = "Catalog对象")
public class Catalog implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	/**
	* 名称
	*/
		@ApiModelProperty(value = "名称")
		private String name;
	/**
	* 描述
	*/
		@ApiModelProperty(value = "描述")
		private String description;
	/**
	* 主题
	*/
		@ApiModelProperty(value = "主题")
		private Long topicId;
	/**
	* 行业
	*/
		@ApiModelProperty(value = "行业")
		private Long industryId;
	/**
	* 共享类型
	*/
		@ApiModelProperty(value = "共享类型")
		private Long shareTypeId;
	/**
	* 更新频率
	*/
		@ApiModelProperty(value = "更新频率")
		private Long updateFrequencyDictId;
	/**
	* 数据源类型
	*/
		@ApiModelProperty(value = "数据源类型")
		private String dataSourceType;
	/**
	* 数据源库名
	*/
		@ApiModelProperty(value = "数据源库名")
		private String dataSourceDb;
	/**
	* 数据源表名
	*/
		@ApiModelProperty(value = "数据源表名")
		private String dataSourceTable;
	private Long createUser;
	private LocalDateTime createTime;
	private Long updateUser;
	private LocalDateTime updateTime;
	private Integer isDeleted;
	private Integer dataFieldCount;
	private Long dataRecordCount;


}
