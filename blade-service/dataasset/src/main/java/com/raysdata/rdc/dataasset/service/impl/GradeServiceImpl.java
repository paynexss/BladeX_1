/*
 *      Copyright (c) 2018-2028, Chill Zhuang All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the dreamlu.net developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: Chill 庄骞 (smallchill@163.com)
 */
package com.raysdata.rdc.dataasset.service.impl;

import com.raysdata.rdc.dataasset.entity.Grade;
import com.raysdata.rdc.dataasset.vo.GradeVO;
import com.raysdata.rdc.dataasset.mapper.GradeMapper;
import com.raysdata.rdc.dataasset.service.IGradeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 *  服务实现类
 *
 * @author BladeX
 * @since 2021-06-03
 */
@Service
public class GradeServiceImpl extends ServiceImpl<GradeMapper, Grade> implements IGradeService {

	@Override
	public IPage<GradeVO> selectGradePage(IPage<GradeVO> page, GradeVO grade) {
		return page.setRecords(baseMapper.selectGradePage(page, grade));
	}

}
