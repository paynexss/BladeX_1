/*
 *      Copyright (c) 2018-2028, Chill Zhuang All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the dreamlu.net developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: Chill 庄骞 (smallchill@163.com)
 */
package com.raysdata.rdc.dataasset.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import lombok.AllArgsConstructor;
import javax.validation.Valid;

import org.springblade.core.mp.support.Condition;
import org.springblade.core.mp.support.Query;
import org.springblade.core.tool.api.R;
import org.springblade.core.tool.utils.BeanUtil;
import org.springblade.core.tool.utils.Func;
import org.springblade.system.user.entity.User;
import org.springblade.system.user.feign.IUserClient;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.raysdata.rdc.dataasset.entity.Grade;
import com.raysdata.rdc.dataasset.vo.GradeVO;
import com.raysdata.rdc.dataasset.service.IGradeService;
import org.springblade.core.boot.ctrl.BladeController;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 *  控制器
 *
 * @author BladeX
 * @since 2021-06-03
 */
@RestController
@AllArgsConstructor
@RequestMapping("/grade")
@Api(value = "", tags = "资源分级接口")
public class GradeController extends BladeController {

	private final IGradeService gradeService;
	private final IUserClient userClient;

	/**
	 * 详情
	 */
	@GetMapping("/detail")
	@ApiOperationSupport(order = 1)
	@ApiOperation(value = "详情", notes = "传入grade")
	public R<Grade> detail(Grade grade) {
		Grade detail = gradeService.getOne(Condition.getQueryWrapper(grade));
		return R.data(detail);
	}

	/**
	 * 分页 
	 */
	@GetMapping("/list")
	@ApiOperationSupport(order = 2)
	@ApiOperation(value = "列表", notes = "传入grade")
	public R<IPage<Grade>> list(Grade grade, Query query) {
		IPage<Grade> pages = gradeService.page(Condition.getPage(query), Condition.getQueryWrapper(grade));
		return R.data(pages);
	}

	/**
	 * 自定义分页 
	 */
	@GetMapping("/page")
	@ApiOperationSupport(order = 3)
	@ApiOperation(value = "分页", notes = "传入grade")
	public R<IPage<GradeVO>> page(GradeVO grade, Query query) {
		IPage<GradeVO> pages = gradeService.selectGradePage(Condition.getPage(query), grade);
		List<GradeVO> voList=new ArrayList<>();
		for(Grade entry:pages.getRecords()){
			GradeVO vo=BeanUtil.copyProperties(entry,GradeVO.class);
			R<User> r=userClient.userInfoById(vo.getCreateUser());
			User user=r.getData();
			vo.setCreateUserName(Optional.ofNullable(user).map(u->u.getAccount()).orElse(""));
			voList.add(vo);
		}
		pages.setRecords(voList);
		return R.data(pages);
	}

	/**
	 * 新增 
	 */
	@PostMapping("/save")
	@ApiOperationSupport(order = 4)
	@ApiOperation(value = "新增", notes = "传入grade")
	public R save(@Valid @RequestBody Grade grade) {
		grade.setCreateTime(LocalDateTime.now());
		grade.setUpdateTime(LocalDateTime.now());
		grade.setCreateUser(getUser().getUserId());
		grade.setUpdateUser(getUser().getUserId());
		return R.status(gradeService.save(grade));
	}

	/**
	 * 修改 
	 */
	@PostMapping("/update")
	@ApiOperationSupport(order = 5)
	@ApiOperation(value = "修改", notes = "传入grade")
	public R update(@Valid @RequestBody Grade grade) {
		grade.setUpdateTime(LocalDateTime.now());
		grade.setUpdateUser(getUser().getUserId());
		return R.status(gradeService.updateById(grade));
	}

	/**
	 * 新增或修改 
	 */
	@PostMapping("/submit")
	@ApiOperationSupport(order = 6)
	@ApiOperation(value = "新增或修改", notes = "传入grade")
	public R submit(@Valid @RequestBody Grade grade) {
		if(grade.getId()==null || grade.getId().equals(0L)){
			grade.setCreateUser(getUser().getUserId());
			grade.setCreateTime(LocalDateTime.now());
		}
		grade.setUpdateUser(getUser().getUserId());
		grade.setUpdateTime(LocalDateTime.now());
		return R.status(gradeService.saveOrUpdate(grade));
	}

	
	/**
	 * 删除 
	 */
	@PostMapping("/remove")
	@ApiOperationSupport(order = 8)
	@ApiOperation(value = "删除", notes = "传入ids")
	public R remove(@ApiParam(value = "主键集合", required = true) @RequestParam String ids) {
		return R.status(gradeService.removeByIds(Func.toLongList(ids)));
	}

	
}
