/*
 *      Copyright (c) 2018-2028, Chill Zhuang All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the dreamlu.net developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: Chill 庄骞 (smallchill@163.com)
 */
package com.raysdata.rdc.dataasset.controller;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.raysdata.rdc.dataasset.dto.LabelResourceRelationDTO;
import com.raysdata.rdc.dataasset.entity.Label;
import com.raysdata.rdc.dataasset.entity.Topic;
import com.raysdata.rdc.dataasset.enums.LabelRelationEnum;
import com.raysdata.rdc.dataasset.service.ILabelService;
import com.raysdata.rdc.dataasset.wrapper.LabelResourceRelationWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import lombok.AllArgsConstructor;
import javax.validation.Valid;

import org.springblade.core.mp.support.Condition;
import org.springblade.core.mp.support.Query;
import org.springblade.core.tool.api.R;
import org.springblade.core.tool.utils.Func;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.raysdata.rdc.dataasset.entity.LabelResourceRelation;
import com.raysdata.rdc.dataasset.vo.LabelResourceRelationVO;
import com.raysdata.rdc.dataasset.service.ILabelResourceRelationService;
import org.springblade.core.boot.ctrl.BladeController;
import springfox.documentation.annotations.ApiIgnore;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 *  控制器
 *
 * @author BladeX
 * @since 2021-05-15
 */
@RestController
@AllArgsConstructor
@RequestMapping("/labelResourceRelation")
@Api(value = "", tags = "标签与资源绑定相关接口")
public class LabelResourceRelationController extends BladeController {

	private final ILabelResourceRelationService labelResourceRelationService;
	private final ILabelService labelService;

	/**
	 * 详情
	 */
	@GetMapping("/detail")
	@ApiOperationSupport(order = 1)
	@ApiOperation(value = "详情", notes = "传入labelResourceRelation")
	public R<LabelResourceRelation> detail(LabelResourceRelation labelResourceRelation) {
		LabelResourceRelation detail = labelResourceRelationService.getOne(Condition.getQueryWrapper(labelResourceRelation));
		return R.data(detail);
	}

	/**
	 * 分页 
	 */
	@GetMapping("/list")
	@ApiOperationSupport(order = 2)
	@ApiOperation(value = "列表", notes = "传入labelResourceRelation")
	public R<IPage<LabelResourceRelationVO>> list(LabelResourceRelation labelResourceRelation, Query query) {
		IPage<LabelResourceRelation> pages = labelResourceRelationService.page(Condition.getPage(query), Condition.getQueryWrapper(labelResourceRelation).lambda());
		return R.data(LabelResourceRelationWrapper.build().pageVO(pages));
	}

	/**
	 * 自定义分页 
	 */
	@GetMapping("/page")
	@ApiOperationSupport(order = 3)
	@ApiOperation(value = "分页", notes = "传入labelResourceRelation")
	public R<IPage<LabelResourceRelationVO>> page(LabelResourceRelationVO labelResourceRelation, Query query) {
		IPage<LabelResourceRelationVO> pages = labelResourceRelationService.selectLabelResourceRelationPage(Condition.getPage(query), labelResourceRelation);
		for(LabelResourceRelationVO relation:pages.getRecords()){
			Label label=labelService.getById(relation.getLabelId());
			Optional.ofNullable(label).ifPresent(l->relation.setLabelName(l.getName()));
		}
		return R.data(pages);
	}

	/**
	 * 新增 
	 */
	@PostMapping("/save")
	@ApiOperationSupport(order = 4)
	@ApiOperation(value = "新增", notes = "传入labelResourceRelation")
	public R save(@Valid @RequestBody LabelResourceRelation labelResourceRelation) {
		labelResourceRelation.setCreateUser(getUser().getUserId());
		labelResourceRelation.setCreateTime(LocalDateTime.now());
		return R.status(labelResourceRelationService.save(labelResourceRelation));
	}

	@PostMapping("/saveBatchResource")
	@ApiOperationSupport(order = 4)
	@ApiOperation(value = "标签批量绑定资源", notes = "传入labelResourceRelation")
	public R saveBatchResource(@Valid @RequestBody LabelResourceRelationDTO labelResourceRelationDTO) {
		labelResourceRelationService.remove(Wrappers.<LabelResourceRelation>query().lambda().eq(LabelResourceRelation::getResourceType, labelResourceRelationDTO.getResourceType()).eq(LabelResourceRelation::getLabelId,labelResourceRelationDTO.getLabelId()));
		List<LabelResourceRelation> list=new ArrayList();
		if(labelResourceRelationDTO.getResourceList()!=null){
			for(Long resourceId:labelResourceRelationDTO.getResourceList()){
				LabelResourceRelation entry=new LabelResourceRelation();
				entry.setResourceId(resourceId);
				entry.setResourceType(labelResourceRelationDTO.getResourceType());
				entry.setLabelId(labelResourceRelationDTO.getLabelId());
				entry.setCreateUser(getUser().getUserId());
				entry.setCreateTime(LocalDateTime.now());
				list.add(entry);
			}
		}
		return R.status(labelResourceRelationService.saveBatch(list));
	}

	@PostMapping("/saveBatchLabel")
	@ApiOperationSupport(order = 4)
	@ApiOperation(value = "资源批量绑定标签", notes = "传入LabelResourceRelationDTO")
	public R saveBatchLabel(@Valid @RequestBody LabelResourceRelationDTO labelResourceRelationDTO) {
		labelResourceRelationService.remove(Wrappers.<LabelResourceRelation>query().lambda().eq(LabelResourceRelation::getResourceType, labelResourceRelationDTO.getResourceType()).eq(LabelResourceRelation::getResourceId,labelResourceRelationDTO.getResourceId()));
		List<LabelResourceRelation> list=new ArrayList();
		if(labelResourceRelationDTO.getLabelList()!=null){
			for(Long labelId:labelResourceRelationDTO.getLabelList()){
				LabelResourceRelation entry=new LabelResourceRelation();
				entry.setLabelId(labelId);
				entry.setResourceType(labelResourceRelationDTO.getResourceType());
				entry.setResourceId(labelResourceRelationDTO.getResourceId());
				entry.setCreateUser(getUser().getUserId());
				entry.setCreateTime(LocalDateTime.now());
				list.add(entry);
			}
		}
		return R.status(labelResourceRelationService.saveBatch(list));
	}

	/**
	 * 修改 
	 */
	@PostMapping("/update")
	@ApiOperationSupport(order = 5)
	@ApiOperation(value = "修改", notes = "传入labelResourceRelation")
	public R update(@Valid @RequestBody LabelResourceRelation labelResourceRelation) {
		return R.status(labelResourceRelationService.updateById(labelResourceRelation));
	}

	/**
	 * 新增或修改 
	 */
	@PostMapping("/submit")
	@ApiOperationSupport(order = 6)
	@ApiOperation(value = "新增或修改", notes = "传入labelResourceRelation")
	public R submit(@Valid @RequestBody LabelResourceRelation labelResourceRelation) {
		labelResourceRelation.setCreateUser(getUser().getUserId());
		labelResourceRelation.setCreateTime(LocalDateTime.now());
		return R.status(labelResourceRelationService.saveOrUpdate(labelResourceRelation));
	}

	
	/**
	 * 删除 
	 */
	@PostMapping("/remove")
	@ApiOperationSupport(order = 8)
	@ApiOperation(value = "删除", notes = "传入ids")
	public R remove(@ApiParam(value = "主键集合", required = true) @RequestParam String ids) {
		return R.status(labelResourceRelationService.removeByIds(Func.toLongList(ids)));
	}

	
}
