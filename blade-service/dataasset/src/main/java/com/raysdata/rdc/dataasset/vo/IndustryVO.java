package com.raysdata.rdc.dataasset.vo;

import com.raysdata.rdc.dataasset.entity.Industry;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;

/**
 * 视图实体类
 *
 * @author mengjingji
 * @since 2021-04-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "IndustryVO对象", description = "IndustryVO对象")
public class IndustryVO extends Industry {
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "资源目录数量")
	Integer catalogCount;
	@ApiModelProperty(value = "信息项数量")
	Integer dataFieldCount;

}
