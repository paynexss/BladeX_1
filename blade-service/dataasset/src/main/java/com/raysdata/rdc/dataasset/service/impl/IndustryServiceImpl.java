package com.raysdata.rdc.dataasset.service.impl;

import com.raysdata.rdc.dataasset.entity.Industry;
import com.raysdata.rdc.dataasset.vo.IndustryVO;
import com.raysdata.rdc.dataasset.mapper.IndustryMapper;
import com.raysdata.rdc.dataasset.service.IIndustryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 *  服务实现类
 *
 * @author mengjingji
 * @since 2021-04-28
 */
@Service
public class IndustryServiceImpl extends ServiceImpl<IndustryMapper, Industry> implements IIndustryService {

	@Override
	public IPage<IndustryVO> selectIndustryPage(IPage<IndustryVO> page, IndustryVO industry) {
		return page.setRecords(baseMapper.selectIndustryPage(page, industry));
	}

}
