package com.raysdata.rdc.dataasset.mapper;

import com.raysdata.rdc.dataasset.entity.Topic;
import com.raysdata.rdc.dataasset.vo.TopicVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;

/**
 *  Mapper 接口
 *
 * @author mengjingji
 * @since 2021-04-28
 */
public interface TopicMapper extends BaseMapper<Topic> {

	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param topic
	 * @return
	 */
	List<TopicVO> selectTopicPage(IPage page, TopicVO topic);

}
