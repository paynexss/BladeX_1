/*
 *      Copyright (c) 2018-2028, Chill Zhuang All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the dreamlu.net developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: Chill 庄骞 (smallchill@163.com)
 */
package com.raysdata.rdc.dataasset.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 实体类
 *
 * @author BladeX
 * @since 2021-05-02
 */
@Data
@TableName("da_data_share")
@ApiModel(value = "DataShare对象", description = "DataShare对象")
public class DataShare implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	/**
	* 使用方
	*/
		@ApiModelProperty(value = "使用方")
		private String useDepartment;
	/**
	* 数据源库名
	*/
		@ApiModelProperty(value = "数据源库名")
		private String dataSourceDb;
	/**
	* 数据源类型
	*/
		@ApiModelProperty(value = "数据源类型")
		private String dataSourceType;
	/**
	* 数据源表名
	*/
		@ApiModelProperty(value = "数据源表名")
		private String dataSourceTable;
	/**
	* 接口地址
	*/
		@ApiModelProperty(value = "接口地址")
		private String name;
	/**
	* 接口说明
	*/
		@ApiModelProperty(value = "接口说明")
		private String description;
	/**
	* 入参
	*/
		@ApiModelProperty(value = "入参")
		private String inputParams;
	/**
	* 返回参数
	*/
		@ApiModelProperty(value = "返回参数")
		private String outputParams;
	private String token;
	/**
	* 调用次数
	*/
		@ApiModelProperty(value = "调用次数")
		private Long callCount;
	/**
	* 调用数据量
	*/
		@ApiModelProperty(value = "调用数据量")
		private Long callRecords;
	/**
	 * 提供方
	 */
	@ApiModelProperty(value = "提供方")
	private String supplyDepartment;
	/**
	 * 提供时间
	 */
	@ApiModelProperty(value = "提供时间")
	private LocalDateTime supplyTime;
	private Long createUser;
	private LocalDateTime createTime;
	private Long updateUser;
	private LocalDateTime updateTime;
	private Integer isDeleted;


}
