package com.raysdata.rdc.dataasset.mapper;

import com.raysdata.rdc.dataasset.entity.Industry;
import com.raysdata.rdc.dataasset.vo.IndustryVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;

/**
 *  Mapper 接口
 *
 * @author mengjingji
 * @since 2021-04-28
 */
public interface IndustryMapper extends BaseMapper<Industry> {

	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param industry
	 * @return
	 */
	List<IndustryVO> selectIndustryPage(IPage page, IndustryVO industry);

}
