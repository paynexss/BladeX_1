package com.raysdata.rdc.dataasset.wrapper;

import com.raysdata.rdc.dataasset.entity.*;
import com.raysdata.rdc.dataasset.enums.LabelRelationEnum;
import com.raysdata.rdc.dataasset.service.ICatalogService;
import com.raysdata.rdc.dataasset.service.ILabelResourceRelationService;
import com.raysdata.rdc.dataasset.service.ILabelService;
import com.raysdata.rdc.dataasset.utils.GetBeanUtil;
import com.raysdata.rdc.dataasset.vo.LabelResourceRelationVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springblade.core.mp.support.BaseEntityWrapper;
import org.springblade.core.tool.api.R;
import org.springblade.core.tool.utils.BeanUtil;
import org.springblade.core.tool.utils.SpringUtil;
import org.springblade.system.user.entity.User;
import org.springblade.system.user.feign.IUserClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Optional;

@Component
public class LabelResourceRelationWrapper   extends BaseEntityWrapper<LabelResourceRelation, LabelResourceRelationVO> {
	private static final Logger logger = LoggerFactory.getLogger(LabelResourceRelationWrapper.class);
	@Autowired
	private  ILabelResourceRelationService labelResourceRelationService;
	@Autowired
	private  ILabelService labelService;
	@Autowired
	private ICatalogService catalogService;

	private static   IUserClient userClient ;
	public static LabelResourceRelationWrapper build() {

		userClient=SpringUtil.getBean(IUserClient.class);
		return	GetBeanUtil.getBean(LabelResourceRelationWrapper.class);
	}
	@Override
	public LabelResourceRelationVO entityVO(LabelResourceRelation o) {
		LabelResourceRelationVO vo = Objects.requireNonNull(BeanUtil.copy(o, LabelResourceRelationVO.class));
		Label label=labelService.getById(vo.getLabelId());
		Optional.ofNullable(label).ifPresent(l->vo.setLabelName(l.getName()));
		R<User> createUser=userClient.userInfoById(o.getCreateUser());
		try{
			Optional.ofNullable(createUser.getData()).ifPresent(u->vo.setCreateUserName(u.getAccount()));
		}catch(ClassCastException e){
			logger.debug("not found user!");
		}


		switch (LabelRelationEnum.valueOf(vo.getResourceType())){
			case database:{
				break;
			}
			case catalog:{
				Catalog catalog=catalogService.getById(vo.getResourceId());
				Optional.ofNullable(catalog).ifPresent(c->vo.setResourceName(c.getName()));
				break;
			}
			case table:{
				break;
			}
		}
		return vo;
	}
}

