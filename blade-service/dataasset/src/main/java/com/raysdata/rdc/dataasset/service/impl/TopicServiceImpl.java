package com.raysdata.rdc.dataasset.service.impl;

import com.raysdata.rdc.dataasset.entity.Topic;
import com.raysdata.rdc.dataasset.vo.TopicVO;
import com.raysdata.rdc.dataasset.mapper.TopicMapper;
import com.raysdata.rdc.dataasset.service.ITopicService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 *  服务实现类
 *
 * @author mengjingji
 * @since 2021-04-28
 */
@Service
public class TopicServiceImpl extends ServiceImpl<TopicMapper, Topic> implements ITopicService {

	@Override
	public IPage<TopicVO> selectTopicPage(IPage<TopicVO> page, TopicVO topic) {
		return page.setRecords(baseMapper.selectTopicPage(page, topic));
	}

}
