package com.raysdata.rdc.dataasset.wrapper;



import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.raysdata.rdc.dataasset.entity.CatalogStatistics;
import com.raysdata.rdc.dataasset.entity.LabelResourceRelation;
import com.raysdata.rdc.dataasset.entity.Topic;
import com.raysdata.rdc.dataasset.enums.CatalogResourceEnum;
import com.raysdata.rdc.dataasset.service.ICatalogStatisticsService;
import com.raysdata.rdc.dataasset.utils.GetBeanUtil;
import com.raysdata.rdc.dataasset.vo.TopicVO;
import lombok.AllArgsConstructor;
import org.springblade.core.mp.support.BaseEntityWrapper;
import org.springblade.core.tool.utils.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;


import java.util.Objects;
import java.util.Optional;

/**
	 * Topic包装类,返回视图层所需的字段
	 *
	 * @author mengjingji
	 */
@Component
public class TopicWrapper extends BaseEntityWrapper<Topic, TopicVO> {
	@Autowired
	private    ICatalogStatisticsService catalogStatisticsService;

	public static TopicWrapper build() {
		return GetBeanUtil.getBean(TopicWrapper.class);
	}

	@Override
	public TopicVO entityVO(Topic notice) {
		TopicVO vo = Objects.requireNonNull(BeanUtil.copy(notice, TopicVO.class));
		//String dictValue = DictCache.getValue(DictEnum.NOTICE, noticeVO.getCategory());
		CatalogStatistics catalogStatistics=catalogStatisticsService.getOne(Wrappers.<CatalogStatistics>query().lambda().eq(CatalogStatistics::getResourceType, CatalogResourceEnum.TOPIC.getName()).eq(CatalogStatistics::getResourceId,vo.getId()));
		Optional.ofNullable(catalogStatistics).ifPresent(c-> {
			vo.setCatalogCount(c.getCatalogCount());
			vo.setDataFieldCount(c.getDataFieldCount());
		});
		return vo;
	}

}

