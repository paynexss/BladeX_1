package com.raysdata.rdc.customdataasset.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.raysdata.rdc.customdataasset.entity.Dict;
import com.raysdata.rdc.customdataasset.vo.FireStatusCount;
import com.raysdata.rdc.customdataasset.vo.RegionStatsInfo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface FireStatisticsMapper extends BaseMapper {

	@Select("SELECT * FROM jqxx_jbxx where status = #{status}")
	List<Dict> selectByName(@Param("status") Integer status);

	@Select("SELECT JQZT_LBDM as code,count(1) as count from jqxx_jbxx GROUP BY JQZT_LBDM;")
	List<FireStatusCount> statAlarmStatus();
	@Select("SELECT JQDJDM as code,count(1) as count from jqxx_jbxx WHERE LENGTH(JQDJDM)>0 GROUP BY JQDJDM order by count desc")
	List<FireStatusCount> statAlarmLevel();

	@Select("SELECT XZQHDM as code,count(1) as count from jqxx_jbxx WHERE LENGTH(XZQHDM)>0 GROUP BY XZQHDM order by count desc limit 10")
	List<FireStatusCount> statRegionTop10();

	@Select("SELECT XZQHDM as code,JQDJDM as level,count(1) as count from jqxx_jbxx WHERE LENGTH(XZQHDM)>0 and LENGTH(JQDJDM)>0  GROUP BY XZQHDM,JQDJDM")
	List<RegionStatsInfo> statRegionLevel();
	@Select("SELECT XZQHDM as code,count(1) as count from jczy_xfmt  GROUP BY XZQHDM union  SELECT XZQHDM as code,count(1) as count from jczy_xhs  GROUP BY XZQHDM union SELECT XZQHDM as code,count(1) as count from sys_dept  GROUP BY XZQHDM union SELECT XZQHDM as code,count(1) as count from jczy_trsy  GROUP BY XZQHDM union SELECT XZQHDM as code,count(1) as count from jczy_xfsh  GROUP BY XZQHDM union SELECT XZQHDM as code,count(1) as count from jczy_lqbzdw  GROUP BY XZQHDM union SELECT XZQHDM as code,count(1) as count from jczy_yjlddw  GROUP BY XZQHDM union SELECT XZQHDM as code,count(1) as count from jczy_mhjyzj  GROUP BY XZQHDM union SELECT XZQHDM as code,count(1) as count from jczy_wxxfz  GROUP BY XZQHDM union SELECT XZQHDM as code,count(1) as count from jczy_zddw  GROUP BY XZQHDM union SELECT XZQHDM as code,count(1) as count from jczy_zddw  GROUP BY XZQHDM")
	List<FireStatusCount> statRegionData();


	@Select("SELECT * from sys_dict where type=#{type} and parent_id<>0")
	List<Dict> selectDict(@Param("type") String  type);
}
