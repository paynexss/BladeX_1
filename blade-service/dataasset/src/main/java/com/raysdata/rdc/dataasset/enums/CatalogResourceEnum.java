package com.raysdata.rdc.dataasset.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CatalogResourceEnum {
	/**
	 * 主题
	 */
	TOPIC("topic"),
	/**
	 * 行业
	 */
	INDUSTRY("industry");

	final String name;
}
