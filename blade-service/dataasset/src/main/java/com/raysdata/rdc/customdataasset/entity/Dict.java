package com.raysdata.rdc.customdataasset.entity;


import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("sys_dict")
public class Dict  implements Serializable {
	private static final long serialVersionUID = 1L;
	Long id;
	String name;
	String value;
	String type;
	String description;



}
