package com.raysdata.rdc.dataasset.utils;

import java.util.Date;

public class TestRS256 {
	public static void test(String[] args) throws InterruptedException {
		TestRS256 t = new TestRS256();
		t.testRS256();
	}

	//测试RS256加密生成Token
	public void testRS256() throws InterruptedException {
		System.out.println(System.currentTimeMillis());
		Long exp=System.currentTimeMillis()+1000L*3600*24*365*10;
		System.out.println(exp);
		System.out.println(new Date(exp));
		String token = JwtUtil.buildToken("account123",exp);
		//解密token
		String account = JwtUtil.volidToken(token);
		System.out.println("校验token成功，token的账号："+account);

		//测试过期
		Thread.sleep(10 * 1000);
		account = JwtUtil.volidToken(token);
		System.out.println(account);
	}
}
