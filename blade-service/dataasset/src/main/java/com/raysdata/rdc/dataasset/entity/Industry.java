package com.raysdata.rdc.dataasset.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 实体类
 *
 * @author mengjingji
 * @since 2021-04-28
 */
@Data
@TableName("da_industry")
@ApiModel(value = "Industry对象", description = "Industry对象")
public class Industry implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "主键",dataType = "Long")
	private Long id;
	@ApiModelProperty(value = "图标")
	private String icon;
	@ApiModelProperty(value = "行业名称",example = "交通")
	private String name;
	@ApiModelProperty(value = "行业描述")
	private String description;
	@ApiModelProperty(hidden = true)
	private Long createUser;
	@ApiModelProperty(hidden = true)
	private LocalDateTime createTime;
	@ApiModelProperty(hidden = true)
	private Long updateUser;
	@ApiModelProperty(hidden = true)
	private LocalDateTime updateTime;
	//@TableLogic
	@ApiModelProperty(hidden = true)
	private Integer isDeleted;


}
