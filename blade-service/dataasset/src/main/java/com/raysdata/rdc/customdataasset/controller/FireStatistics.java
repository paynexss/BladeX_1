package com.raysdata.rdc.customdataasset.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.raysdata.rdc.customdataasset.entity.Dict;
import com.raysdata.rdc.customdataasset.mapper.FireStatisticsMapper;
import com.raysdata.rdc.customdataasset.vo.FireStatusCount;
import com.raysdata.rdc.customdataasset.vo.RegionStatsInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springblade.core.boot.ctrl.BladeController;
import org.springblade.core.tool.api.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/fire-stats")
@Api(value = "消防统计", tags = "消防统计接口")
public class FireStatistics extends BladeController {
	@Autowired
	FireStatisticsMapper fireStatisticsMapper;
	/**
	 * 警情状态统计
	 */
	@GetMapping("/status")
	@ApiOperationSupport(order = 1)
	@ApiOperation(value = "警情状态统计", notes = "警情状态统计")
	public R<Object> status() {

		List<FireStatusCount> list = fireStatisticsMapper.statAlarmStatus();
		List<Dict> dicts = fireStatisticsMapper.selectDict("JQZTLBDM");

		for(FireStatusCount item:list){
			for(Dict dict:dicts){
				if(item.getCode()!=null && item.getCode().equals(dict.getValue())){
					item.setName(dict.getName());
				}
			}
		}

		return R.data(list);
	}

	/**
	 * 警情等级统计
	 */
	@GetMapping("/level")
	@ApiOperationSupport(order = 1)
	@ApiOperation(value = "警情等级统计", notes = "警情等级统计")
	public R<Object> level() {

		List<FireStatusCount> list = fireStatisticsMapper.statAlarmLevel();
		List<Dict> dicts = fireStatisticsMapper.selectDict("JQDJDM");

		for(FireStatusCount item:list){
			for(Dict dict:dicts){
				if(item.getCode()!=null && item.getCode().equals(dict.getValue())){
					item.setName(dict.getName());
				}
			}
		}

		return R.data(list);
	}

	/**
	 * 警情辖区数量排名
	 */
	@GetMapping("/regionTop10")
	@ApiOperationSupport(order = 1)
	@ApiOperation(value = "警情辖区数量排名", notes = "警情辖区数量排名")
	public R<Object> regionTop10() {

		List<FireStatusCount> list = fireStatisticsMapper.statRegionTop10();
		List<Dict> dicts = fireStatisticsMapper.selectDict("XZQHDM");

		for(FireStatusCount item:list){
			for(Dict dict:dicts){
				if(item.getCode()!=null && item.getCode().equals(dict.getValue())){
					item.setName(dict.getName());
				}
			}
		}

		return R.data(list);
	}


	/**
	 * 区域数据统计
	 */
	@GetMapping("/regionStatsInfo")
	@ApiOperationSupport(order = 1)
	@ApiOperation(value = "区域数据统计", notes = "区域数据统计")
	public R<Object> regionStatsInfo() {

		List<RegionStatsInfo> list = fireStatisticsMapper.statRegionLevel();
		List<FireStatusCount> regionData = fireStatisticsMapper.statRegionData();
		List<Dict> dicts = fireStatisticsMapper.selectDict("XZQHDM");

		Map<String,RegionStatsInfo> map=new HashMap<>();
		for(RegionStatsInfo region:list){
			RegionStatsInfo info=map.get(region.getCode());
			if(info==null){
				info=new RegionStatsInfo();
				info.setCode(region.getCode());

				for(Dict dict:dicts){
					if(region.getCode()!=null && region.getCode().equals(dict.getValue())){
						info.setName(dict.getName());
					}
				}
				map.put(info.getCode(),info);
			}
			info.setCount(info.getCount()+region.getCount());
			if("5".equals(region.getLevel())){
				info.setLevel5(info.getLevel5()+region.getCount());
			}
			if("4".equals(region.getLevel())){
				info.setLevel4(info.getLevel4()+region.getCount());
			}
		}

		for(FireStatusCount region:regionData){
			RegionStatsInfo info=map.get(region.getCode());
			if(info==null){
				continue;
			}
			info.setCount(info.getCount()+region.getCount());
		}

		return R.data(map.values());
	}


}
