package com.raysdata.rdc.dataasset.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.raysdata.rdc.dataasset.dto.LabelNode;
import com.raysdata.rdc.dataasset.entity.Industry;
import com.raysdata.rdc.dataasset.entity.Topic;
import com.raysdata.rdc.dataasset.service.IIndustryService;
import com.raysdata.rdc.dataasset.service.ITopicService;
import com.raysdata.rdc.dataasset.vo.IndustryVO;
import com.raysdata.rdc.dataasset.wrapper.LabelWrapper;
import io.swagger.annotations.*;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import lombok.AllArgsConstructor;
import javax.validation.Valid;

import org.springblade.core.mp.support.Condition;
import org.springblade.core.mp.support.Query;
import org.springblade.core.tool.api.R;
import org.springblade.core.tool.utils.BeanUtil;
import org.springblade.core.tool.utils.Func;
import org.springblade.system.entity.DictBiz;
import org.springblade.system.feign.IDictBizClient;
import org.springblade.system.feign.IDictClient;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.raysdata.rdc.dataasset.entity.Label;
import com.raysdata.rdc.dataasset.vo.LabelVO;
import com.raysdata.rdc.dataasset.service.ILabelService;
import org.springblade.core.boot.ctrl.BladeController;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

/**
 *  控制器
 *
 * @author mengjingji
 * @since 2021-05-14
 */
@RestController
@AllArgsConstructor
@RequestMapping("/label")
@Api(value = "", tags = "标签接口")
public class LabelController extends BladeController {

	private final ILabelService labelService;

	/**
	 * 详情
	 */
	@GetMapping("/detail")
	@ApiOperationSupport(order = 1)
	@ApiOperation(value = "详情", notes = "传入labelId")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "id", value = "主键",defaultValue = "1", required = true)
	})
	public R<Label> detail(Long id) {
		Label detail = labelService.getById(id);
		return R.data(detail);
	}

	/**
	 * 列表
	 */
	@GetMapping("/list")
	@ApiOperationSupport(order = 2)
	@ApiOperation(value = "列表", notes = "传入label")
	public List<LabelVO> list(Label label, Query query) {
		List<Label> list = labelService.list(Condition.getQueryWrapper(label));
		return LabelWrapper.build().listVO(list);
	}

	/**
	 * 列表
	 */
	@GetMapping("/tree")
	@ApiOperationSupport(order = 2)
	public LabelNode tree() {
		LabelNode rootNode=new LabelNode();
		rootNode.setLabel(null);
		Label query=new Label();
		query.setParentId(0L);
		List<Label> list = labelService.list(Condition.getQueryWrapper(query));
		List<LabelNode> children=new ArrayList<>();
		for(Label label:list){
			LabelNode node=new LabelNode();
			node.setLabel(LabelWrapper.build().entityVO(label));
			addLabelChildren(node);
			children.add(node);
		}
		rootNode.setChildren(children);
		return rootNode;
	}

	private void addLabelChildren(LabelNode node){
		Label query=new Label();
		query.setParentId(node.getLabel().getId());
		List<Label> list = labelService.list(Condition.getQueryWrapper(query));
		List<LabelNode> children=new ArrayList<>();
		for(Label label:list){
			LabelNode sub=new LabelNode();
			sub.setLabel(LabelWrapper.build().entityVO(label));
			addLabelChildren(sub);
			children.add(sub);
		}
		node.setChildren(children);
	}




	/**
	 * 自定义分页 
	 */
	@GetMapping("/page")
	@ApiOperationSupport(order = 3)
	@ApiOperation(value = "分页", notes = "传入label")
	public R<IPage<LabelVO>> page(Label label, Query query) {
		IPage<Label> pages = labelService.page(Condition.getPage(query), Condition.getQueryWrapper(label));
		//for(LabelVO labelVO: pages.getRecords()){
		//}
		return R.data(LabelWrapper.build().pageVO(pages));
	}

	/**
	 * 新增 
	 */
	@PostMapping("/save")
	@ApiOperationSupport(order = 4)
	@ApiOperation(value = "新增", notes = "传入label")
	public R save(@Valid @RequestBody Label label) {

		label.setCreateUser(getUser().getUserId());
		label.setUpdateUser(getUser().getUserId());
		label.setCreateTime(LocalDateTime.now());
		label.setUpdateTime(LocalDateTime.now());
		label.setTenantId(getUser().getTenantId());
		return R.status(labelService.save(label));
	}

	/**
	 * 修改 
	 */
	@PostMapping("/update")
	@ApiOperationSupport(order = 5)
	@ApiOperation(value = "修改", notes = "传入label")
	public R update(@Valid @RequestBody Label label) {
		label.setUpdateUser(getUser().getUserId());
		label.setUpdateTime(LocalDateTime.now());
		return R.status(labelService.updateById(label));
	}

	/**
	 * 新增或修改 
	 */
	@PostMapping("/submit")
	@ApiOperationSupport(order = 6)
	@ApiOperation(value = "新增或修改", notes = "传入label")
	public R submit(@Valid @RequestBody Label label) {
		if( label.getId()==null){
			label.setCreateUser(getUser().getUserId());
			label.setCreateTime(LocalDateTime.now());
			label.setTenantId(getUser().getTenantId());
		}
		label.setUpdateUser(getUser().getUserId());
		label.setUpdateTime(LocalDateTime.now());
		return R.status(labelService.saveOrUpdate(label));
	}

	
	/**
	 * 删除 
	 */
	@PostMapping("/remove")
	@ApiOperationSupport(order = 8)
	@ApiOperation(value = "删除", notes = "传入ids")
	public R remove(@ApiParam(value = "主键集合", required = true) @RequestParam String ids) {
		return R.status(labelService.removeByIds(Func.toLongList(ids)));
	}

	
}
