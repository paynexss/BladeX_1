package com.raysdata.rdc;

import org.springblade.core.cloud.feign.EnableBladeFeign;
import org.springblade.core.launch.BladeApplication;
import org.springframework.cloud.client.SpringCloudApplication;

/**
 * DataAsset启动器
 *
 * @author mengjingji
 */
@EnableBladeFeign
@SpringCloudApplication
// @SeataCloudApplication
public class DataAssetApplication {
	static String APPLICATION_DATA_ASSET_NAME = "data-asset";

	public static void main(String[] args) {

		BladeApplication.run(APPLICATION_DATA_ASSET_NAME, DataAssetApplication.class, args);
	}

}

