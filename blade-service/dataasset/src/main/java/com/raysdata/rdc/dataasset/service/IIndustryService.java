package com.raysdata.rdc.dataasset.service;

import com.raysdata.rdc.dataasset.entity.Industry;
import com.raysdata.rdc.dataasset.vo.IndustryVO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 *  服务类
 *
 * @author mengjingji
 * @since 2021-04-28
 */
public interface IIndustryService extends IService<Industry> {

	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param industry
	 * @return
	 */
	IPage<IndustryVO> selectIndustryPage(IPage<IndustryVO> page, IndustryVO industry);

}
