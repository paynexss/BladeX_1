/*
 *      Copyright (c) 2018-2028, Chill Zhuang All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the dreamlu.net developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: Chill 庄骞 (smallchill@163.com)
 */
package com.raysdata.rdc.dataasset.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import lombok.AllArgsConstructor;
import javax.validation.Valid;

import org.springblade.core.mp.support.Condition;
import org.springblade.core.mp.support.Query;
import org.springblade.core.tool.api.R;
import org.springblade.core.tool.utils.Func;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.raysdata.rdc.dataasset.entity.CatalogStatistics;
import com.raysdata.rdc.dataasset.vo.CatalogStatisticsVO;
import com.raysdata.rdc.dataasset.service.ICatalogStatisticsService;
import org.springblade.core.boot.ctrl.BladeController;

/**
 *  控制器
 *
 * @author BladeX
 * @since 2021-06-01
 */
@RestController
@AllArgsConstructor
@RequestMapping("/catalogstatistics")
@Api(value = "", tags = "资源目录统计接口")
public class CatalogStatisticsController extends BladeController {

	private final ICatalogStatisticsService catalogStatisticsService;

	/**
	 * 详情
	 */
	@GetMapping("/detail")
	@ApiOperationSupport(order = 1)
	@ApiOperation(value = "详情", notes = "传入catalogStatistics")
	public R<CatalogStatistics> detail(CatalogStatistics catalogStatistics) {
		CatalogStatistics detail = catalogStatisticsService.getOne(Condition.getQueryWrapper(catalogStatistics));
		return R.data(detail);
	}

	/**
	 * 分页 
	 */
	@GetMapping("/list")
	@ApiOperationSupport(order = 2)
	@ApiOperation(value = "分页", notes = "传入catalogStatistics")
	public R<IPage<CatalogStatistics>> list(CatalogStatistics catalogStatistics, Query query) {
		IPage<CatalogStatistics> pages = catalogStatisticsService.page(Condition.getPage(query), Condition.getQueryWrapper(catalogStatistics));
		return R.data(pages);
	}

	/**
	 * 自定义分页 
	 */
	@GetMapping("/page")
	@ApiOperationSupport(order = 3)
	@ApiOperation(value = "分页", notes = "传入catalogStatistics")
	public R<IPage<CatalogStatisticsVO>> page(CatalogStatisticsVO catalogStatistics, Query query) {
		IPage<CatalogStatisticsVO> pages = catalogStatisticsService.selectCatalogStatisticsPage(Condition.getPage(query), catalogStatistics);
		return R.data(pages);
	}

	/**
	 * 新增 
	 */
	@PostMapping("/save")
	@ApiOperationSupport(order = 4)
	@ApiOperation(value = "新增", notes = "传入catalogStatistics")
	public R save(@Valid @RequestBody CatalogStatistics catalogStatistics) {
		return R.status(catalogStatisticsService.save(catalogStatistics));
	}

	/**
	 * 修改 
	 */
	@PostMapping("/update")
	@ApiOperationSupport(order = 5)
	@ApiOperation(value = "修改", notes = "传入catalogStatistics")
	public R update(@Valid @RequestBody CatalogStatistics catalogStatistics) {
		return R.status(catalogStatisticsService.updateById(catalogStatistics));
	}

	/**
	 * 新增或修改 
	 */
	@PostMapping("/submit")
	@ApiOperationSupport(order = 6)
	@ApiOperation(value = "新增或修改", notes = "传入catalogStatistics")
	public R submit(@Valid @RequestBody CatalogStatistics catalogStatistics) {
		return R.status(catalogStatisticsService.saveOrUpdate(catalogStatistics));
	}

	
	/**
	 * 删除 
	 */
	@PostMapping("/remove")
	@ApiOperationSupport(order = 8)
	@ApiOperation(value = "删除", notes = "传入ids")
	public R remove(@ApiParam(value = "主键集合", required = true) @RequestParam String ids) {
		return R.status(catalogStatisticsService.removeByIds(Func.toLongList(ids)));
	}

	
}
