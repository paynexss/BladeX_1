package com.raysdata.rdc.customdataasset;

import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.core.MybatisXMLLanguageDriver;
import com.baomidou.mybatisplus.core.config.GlobalConfig;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

/**
 * mybatis自动扫描配置
 *
 * @author mengjingji
 * @since 2021-04-28
 */
@Configuration
@MapperScan(basePackages="com.raysdata.rdc.customdataasset.mapper.**", sqlSessionTemplateRef  = "ds2SqlSessionTemplate")
public class MybatisplusConfigurationDs2 {


	//ds2数据源
	@Bean("ds2SqlSessionFactory")
	public SqlSessionFactory ds2SqlSessionFactory(@Qualifier("ds2DataSource") DataSource dataSource) throws Exception {
		MybatisSqlSessionFactoryBean sqlSessionFactory = new MybatisSqlSessionFactoryBean();
		sqlSessionFactory.setDataSource(dataSource);
		MybatisConfiguration configuration = new MybatisConfiguration();
		configuration.setDefaultScriptingLanguage(MybatisXMLLanguageDriver.class);
		configuration.setJdbcTypeForNull(JdbcType.NULL);
		sqlSessionFactory.setConfiguration(configuration);
		sqlSessionFactory.setMapperLocations(new PathMatchingResourcePatternResolver().
			getResources("classpath*:com/raysdata/rdc/customdataasset/mapper/**/*.xml"));
		sqlSessionFactory.setPlugins(new Interceptor[]{
		});
		sqlSessionFactory.setGlobalConfig(new GlobalConfig().setBanner(false));
		return sqlSessionFactory.getObject();
	}

	@Bean(name = "ds2TransactionManager")
	public DataSourceTransactionManager ds2TransactionManager(@Qualifier("ds2DataSource") DataSource dataSource) {
		return new DataSourceTransactionManager(dataSource);
	}

	@Bean(name = "ds2SqlSessionTemplate")
	public SqlSessionTemplate ds2SqlSessionTemplate(@Qualifier("ds2SqlSessionFactory") SqlSessionFactory sqlSessionFactory) {
		return new SqlSessionTemplate(sqlSessionFactory);
	}



	//第二个ds2数据源配置
	@Bean(name = "ds2DataSourceProperties")
	@ConfigurationProperties(prefix = "spring.custom-datasource")
	public DataSourceProperties ds2DataSourceProperties() {
		return new DataSourceProperties();
	}
	//第二个ds2数据源
	@Bean("ds2DataSource")
	public DataSource ds2DataSource(@Qualifier("ds2DataSourceProperties") DataSourceProperties dataSourceProperties) {
		return dataSourceProperties.initializeDataSourceBuilder().build();
	}
}
