package com.raysdata.rdc.dataasset.wrapper;

import com.raysdata.rdc.dataasset.entity.Label;
import com.raysdata.rdc.dataasset.service.ILabelService;
import com.raysdata.rdc.dataasset.utils.GetBeanUtil;
import com.raysdata.rdc.dataasset.vo.LabelVO;
import org.springblade.core.mp.support.BaseEntityWrapper;
import org.springblade.core.tool.api.R;
import org.springblade.core.tool.utils.BeanUtil;
import org.springblade.core.tool.utils.SpringUtil;
import org.springblade.system.user.entity.User;
import org.springblade.system.user.feign.IUserClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Optional;

/**
 * Topic包装类,返回视图层所需的字段
 *
 * @author mengjingji
 */
@Component
public class LabelWrapper extends BaseEntityWrapper<Label, LabelVO> {
	@Autowired
	private ILabelService labelService;

	private static IUserClient userClient ;

	public static LabelWrapper build() {
		userClient= SpringUtil.getBean(IUserClient.class);
		return GetBeanUtil.getBean(LabelWrapper.class);
	}

	@Override
	public LabelVO entityVO(Label o) {
		LabelVO vo = Objects.requireNonNull(BeanUtil.copy(o, LabelVO.class));
		//String dictValue = DictCache.getValue(DictEnum.NOTICE, noticeVO.getCategory());
		R<User> createUser=userClient.userInfoById(o.getCreateUser());
		Optional.ofNullable(createUser.getData()).ifPresent(u->vo.setCreateUserName(u.getAccount()));
		return vo;
	}

}

