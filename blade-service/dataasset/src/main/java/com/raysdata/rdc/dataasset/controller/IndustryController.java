package com.raysdata.rdc.dataasset.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.raysdata.rdc.dataasset.entity.CatalogStatistics;
import com.raysdata.rdc.dataasset.entity.Industry;
import com.raysdata.rdc.dataasset.entity.Topic;
import com.raysdata.rdc.dataasset.enums.CatalogResourceEnum;
import com.raysdata.rdc.dataasset.service.ICatalogStatisticsService;
import com.raysdata.rdc.dataasset.service.IIndustryService;
import com.raysdata.rdc.dataasset.vo.IndustryVO;
import com.raysdata.rdc.dataasset.vo.TopicVO;
import io.swagger.annotations.*;
import lombok.AllArgsConstructor;
import org.springblade.core.boot.ctrl.BladeController;
import org.springblade.core.mp.support.Condition;
import org.springblade.core.mp.support.Query;
import org.springblade.core.tool.api.R;
import org.springblade.core.tool.utils.BeanUtil;
import org.springblade.core.tool.utils.Func;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;


/**
 *  控制器
 *
 * @author mengjngji
 * @since 2021-04-28
 */
@RestController
@AllArgsConstructor
@RequestMapping("/industry")
@Api(value = "行业", tags = "行业接口")
public class IndustryController extends BladeController {

	private final IIndustryService industryService;
	private final ICatalogStatisticsService catalogStatisticsService;

	/**
	 * 详情
	 */
	/*@GetMapping("/detail")
	@ApiOperationSupport(order = 1)
	@ApiOperation(value = "详情", notes = "传入industry")
	public R<Industry> detail(Industry industry) {
		Industry detail = industryService.getOne(Condition.getQueryWrapper(industry));
		return R.data(detail);
	}*/

	@GetMapping("/detail")
	@ApiOperationSupport(order = 1)
	@ApiOperation(value = "详情", notes = "传入industryId")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "id", value = "主键",defaultValue = "1", required = true)
	})
	public R<Industry> detail(Long id) {
		Industry detail = industryService.getById(id);
		return R.data(detail);
	}

	/**
	 * 分页
	 */
	@GetMapping("/list")
	@ApiOperationSupport(order = 2)
	@ApiOperation(value = "列表", notes = "传入industry")
	public R<List<IndustryVO>> list(Industry industry) {
		List<Industry> list = industryService.list(Condition.getQueryWrapper(industry));
		List<IndustryVO> listVO=new ArrayList<IndustryVO>();
		for(Industry industry1: list){
			IndustryVO vo = BeanUtil.copyProperties(industry1,IndustryVO.class);
			CatalogStatistics catalogStatistics=catalogStatisticsService.getOne(Wrappers.<CatalogStatistics>query().lambda().eq(CatalogStatistics::getResourceId, industry1.getId()).eq(CatalogStatistics::getResourceType, CatalogResourceEnum.INDUSTRY.getName()));
			vo.setCatalogCount(Optional.ofNullable(catalogStatistics).map(i->i.getCatalogCount()).orElse(0));
			vo.setDataFieldCount(Optional.ofNullable(catalogStatistics).map(i->i.getDataFieldCount()).orElse(0));
			listVO.add(vo);
		}
		return R.data(listVO);
	}

	/**
	 * 自定义分页
	 */
	@GetMapping("/page")
	@ApiOperationSupport(order = 3)
	@ApiOperation(value = "分页", notes = "传入industry")
	public R<IPage<IndustryVO>> page(IndustryVO industry, Query query) {
		IPage<IndustryVO> pages = industryService.selectIndustryPage(Condition.getPage(query), industry);
		return R.data(pages);
	}

	/**
	 * 新增
	 */
	@PostMapping("/save")
	@ApiOperationSupport(order = 4)
	@ApiOperation(value = "新增", notes = "传入industry")
	public R save(@Valid @RequestBody Industry industry) {
		return R.status(industryService.save(industry));
	}

	/**
	 * 修改
	 */
	@PostMapping("/update")
	@ApiOperationSupport(order = 5)
	@ApiOperation(value = "修改", notes = "传入industry")
	/*@ApiImplicitParams({
		@ApiImplicitParam(paramType = "body",name = "id", value = "主键",dataType = "long", required = true, defaultValue = "")
	})*/
	@ApiImplicitParam(name = "industry" , paramType = "body",examples = @Example({
		@ExampleProperty(value = "{'user':'id'}", mediaType = "application/json")
	}))
	public R update(@Valid @RequestBody Industry industry) {
		return R.status(industryService.updateById(industry));
	}

	/**
	 * 新增或修改
	 */
	@PostMapping("/submit")
	@ApiOperationSupport(order = 6)
	@ApiOperation(value = "新增或修改", notes = "传入industry")
	public R submit(@Valid @RequestBody Industry industry) {
		return R.status(industryService.saveOrUpdate(industry));
	}


	/**
	 * 删除
	 */
	@PostMapping("/remove")
	@ApiOperationSupport(order = 8)
	@ApiOperation(value = "删除", notes = "传入ids")
	public R remove(@ApiParam(value = "主键集合", required = true) @RequestParam String ids) {
		return R.status(industryService.removeByIds(Func.toLongList(ids)));
	}



}
