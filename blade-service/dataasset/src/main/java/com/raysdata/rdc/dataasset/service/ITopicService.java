package com.raysdata.rdc.dataasset.service;

import com.raysdata.rdc.dataasset.entity.Topic;
import com.raysdata.rdc.dataasset.vo.TopicVO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 *  服务类
 *
 * @author mengjingji
 * @since 2021-04-28
 */
public interface ITopicService extends IService<Topic> {

	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param topic
	 * @return
	 */
	IPage<TopicVO> selectTopicPage(IPage<TopicVO> page, TopicVO topic);

}
