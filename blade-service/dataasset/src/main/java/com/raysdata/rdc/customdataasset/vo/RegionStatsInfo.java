package com.raysdata.rdc.customdataasset.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class RegionStatsInfo  implements Serializable {
	@ApiModelProperty(value = "名称")
	String name;
	@ApiModelProperty(value = "行政区划代码")
	String code;
	@ApiModelProperty(hidden = true)
	String level;
	@ApiModelProperty(value = "数据量")
	Long count=0L;
	@ApiModelProperty(value = "警情")
	Long alarm=0L;
	@ApiModelProperty(value = "特大警情")
	Long level5=0L;
	@ApiModelProperty(value = "重大警情")
	Long level4=0L;
}
