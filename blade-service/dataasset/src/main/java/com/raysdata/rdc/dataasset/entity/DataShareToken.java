package com.raysdata.rdc.dataasset.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@ApiModel(value = "DataShareToken对象", description = "DataShareToken对象")
public class DataShareToken implements Serializable {
	private Date beginTime;
	private Date endTime;
	private Long dataShareId;
	private String value;

}
