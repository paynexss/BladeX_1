package com.raysdata.rdc.dataasset.dto;

import com.raysdata.rdc.dataasset.entity.Topic;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 数据传输对象实体类
 *
 * @author mengjingji
 * @since 2021-04-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TopicDTO extends Topic {
	private static final long serialVersionUID = 1L;

}
