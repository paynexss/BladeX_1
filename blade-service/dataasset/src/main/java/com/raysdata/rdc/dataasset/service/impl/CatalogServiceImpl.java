/*
 *      Copyright (c) 2018-2028, Chill Zhuang All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the dreamlu.net developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: Chill 庄骞 (smallchill@163.com)
 */
package com.raysdata.rdc.dataasset.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.raysdata.rdc.dataasset.entity.Catalog;
import com.raysdata.rdc.dataasset.entity.CatalogStatistics;
import com.raysdata.rdc.dataasset.vo.CatalogVO;
import com.raysdata.rdc.dataasset.mapper.CatalogMapper;
import com.raysdata.rdc.dataasset.service.ICatalogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.raysdata.rdc.dataasset.wrapper.CatalogWrapper;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;

import java.math.BigDecimal;
import java.sql.Wrapper;
import java.util.ArrayList;
import java.util.List;

/**
 *  服务实现类
 *
 * @author BladeX
 * @since 2021-04-29
 */
@Service
public class CatalogServiceImpl extends ServiceImpl<CatalogMapper, Catalog> implements ICatalogService {

	@Override
	public IPage<CatalogVO> selectCatalogPage(IPage<CatalogVO> page, CatalogVO catalog) {
		return page.setRecords(baseMapper.selectCatalogPage(page, catalog));
	}

	public List<Object> statisticsCatalogShareTypeRatio(){
		List<Object> list=new ArrayList<Object>();
		JSONObject item=new JSONObject();
		item.put("name","有条件共享");
		item.put("count",25);
		item.put("ratio",new BigDecimal("25.0"));
		list.add(item);

		item=new JSONObject();
		item.put("name","无条件共享");
		item.put("count",25);
		item.put("ratio",new BigDecimal("25.0"));
		list.add(item);

		item=new JSONObject();
		item.put("name","不共享");
		item.put("count",50);
		item.put("ratio",new BigDecimal("50.0"));

		list.add(item);

		return list;
	}

	@Override
	public List<CatalogVO> top10() {
		List<Catalog> list=list(Wrappers.<Catalog>query().orderByDesc("data_record_count").last(" limit 10"));
		return CatalogWrapper.build().listVO(list);
	}

}
