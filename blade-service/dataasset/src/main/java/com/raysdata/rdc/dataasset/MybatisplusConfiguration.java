package com.raysdata.rdc.dataasset;

import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.core.MybatisXMLLanguageDriver;
import com.baomidou.mybatisplus.core.config.GlobalConfig;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

/**
 * mybatis自动扫描配置
 *
 * @author mengjingji
 * @since 2021-04-28
 */
@Configuration
@MapperScan(basePackages="com.raysdata.rdc.dataasset.mapper.**" ,sqlSessionTemplateRef  = "ds1SqlSessionTemplate")
public class MybatisplusConfiguration {

	//主数据源 ds1数据源
	@Primary
	@Bean("ds1SqlSessionFactory")
	public SqlSessionFactory ds1SqlSessionFactory(@Qualifier("ds1DataSource") DataSource dataSource) throws Exception {
		MybatisSqlSessionFactoryBean sqlSessionFactory = new MybatisSqlSessionFactoryBean();
		sqlSessionFactory.setDataSource(dataSource);
		MybatisConfiguration configuration = new MybatisConfiguration();
		configuration.setDefaultScriptingLanguage(MybatisXMLLanguageDriver.class);
		configuration.setJdbcTypeForNull(JdbcType.NULL);
		sqlSessionFactory.setConfiguration(configuration);
		sqlSessionFactory.setMapperLocations(new PathMatchingResourcePatternResolver().
			getResources("classpath*:com/raysdata/rdc/dataasset/mapper/**/*.xml"));
		sqlSessionFactory.setPlugins(new Interceptor[]{
		});
		sqlSessionFactory.setGlobalConfig(new GlobalConfig().setBanner(false));
		return sqlSessionFactory.getObject();
	}

	@Primary
	@Bean(name = "ds1TransactionManager")
	public DataSourceTransactionManager ds1TransactionManager(@Qualifier("ds1DataSource") DataSource dataSource) {
		return new DataSourceTransactionManager(dataSource);
	}

	@Primary
	@Bean(name = "ds1SqlSessionTemplate")
	public SqlSessionTemplate ds1SqlSessionTemplate(@Qualifier("ds1SqlSessionFactory") SqlSessionFactory sqlSessionFactory) {
		return new SqlSessionTemplate(sqlSessionFactory);
	}


	//主数据源配置 ds1数据源
	@Primary
	@Bean(name = "ds1DataSourceProperties")
	@ConfigurationProperties(prefix = "spring.datasource")
	public DataSourceProperties ds1DataSourceProperties() {
		return new DataSourceProperties();
	}

	//主数据源 ds1数据源
	@Primary
	@Bean(name = "ds1DataSource")
	public DataSource ds1DataSource(@Qualifier("ds1DataSourceProperties") DataSourceProperties dataSourceProperties) {
		return dataSourceProperties.initializeDataSourceBuilder().build();
	}
}
