package com.raysdata.rdc.customdataasset.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class FireStatusCount implements Serializable {
	String name;
	Long count;
	String code;
}
