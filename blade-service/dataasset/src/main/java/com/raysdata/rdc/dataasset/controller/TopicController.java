package com.raysdata.rdc.dataasset.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.raysdata.rdc.dataasset.entity.CatalogStatistics;
import com.raysdata.rdc.dataasset.enums.CatalogResourceEnum;
import com.raysdata.rdc.dataasset.service.ICatalogStatisticsService;
import com.raysdata.rdc.dataasset.utils.GetBeanUtil;
import com.raysdata.rdc.dataasset.wrapper.TopicWrapper;
import io.swagger.annotations.*;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import lombok.AllArgsConstructor;
import javax.validation.Valid;

import org.springblade.core.mp.support.Condition;
import org.springblade.core.mp.support.Query;
import org.springblade.core.tool.api.R;
import org.springblade.core.tool.utils.BeanUtil;
import org.springblade.core.tool.utils.Func;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.raysdata.rdc.dataasset.entity.Topic;
import com.raysdata.rdc.dataasset.vo.TopicVO;
import com.raysdata.rdc.dataasset.service.ITopicService;
import org.springblade.core.boot.ctrl.BladeController;
import springfox.documentation.annotations.ApiIgnore;

import java.util.*;

/**
 *  控制器
 *
 * @author mengjngji
 * @since 2021-04-28
 */
@RestController
@AllArgsConstructor
@RequestMapping("/topic")
@Api(value = "主题", tags = "主题接口")
public class TopicController extends BladeController {

	private final ITopicService topicService;
	private final ICatalogStatisticsService catalogStatisticsService;

	/**
	 * 详情
	 */
	/*@GetMapping("/detail")
	@ApiOperationSupport(order = 1)
	@ApiOperation(value = "详情", notes = "传入topic")
	public R<Topic> detail(Topic topic) {
		Topic detail = topicService.getOne(Condition.getQueryWrapper(topic));
		return R.data(detail);
	}*/

	@GetMapping("/detail")
	@ApiOperationSupport(order = 1)
	@ApiOperation(value = "详情", notes = "传入topicId")
	public R<Topic> detail(@ApiParam(value = "主键", required = true) @RequestParam Long id) {
		Topic detail = topicService.getById(id);
		return R.data(detail);
	}

	/**
	 * 列表
	 */
	@GetMapping("/list")
	@ApiOperationSupport(order = 2)
	@ApiOperation(value = "列表", notes = "传入topic")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "name", value = "主题名称",defaultValue = "", required = false),
		@ApiImplicitParam(name = "description", value = "主题描述",defaultValue = "", required = false)
	})
	public R<List<TopicVO>> list(@ApiIgnore Topic topic) {
		List<Topic> list = topicService.list(Condition.getQueryWrapper(topic));
		List<TopicVO> listVO=new ArrayList<TopicVO>();
		for(Topic topic1: list){
			TopicVO vo = BeanUtil.copyProperties(topic1,TopicVO.class);
			CatalogStatistics catalogStatistics=catalogStatisticsService.getOne(Wrappers.<CatalogStatistics>query().lambda().eq(CatalogStatistics::getResourceId, topic1.getId()).eq(CatalogStatistics::getResourceType, CatalogResourceEnum.TOPIC.getName()));
			vo.setCatalogCount(Optional.ofNullable(catalogStatistics).map(i->i.getCatalogCount()).orElse(0));
			vo.setDataFieldCount(Optional.ofNullable(catalogStatistics).map(i->i.getDataFieldCount()).orElse(0));
			listVO.add(vo);
		}
		return R.data(listVO);
	}

	/**
	 * 自定义分页 
	 */
	@GetMapping("/page")
	@ApiOperationSupport(order = 3)
	@ApiOperation(value = "分页", notes = "传入topic")
	public R<IPage<TopicVO>> page(@ApiIgnore @RequestParam Map<String, Object> topic, Query query) {
		IPage<Topic> pages = topicService.page(Condition.getPage(query), Condition.getQueryWrapper(topic,Topic.class));
		return R.data(TopicWrapper.build().pageVO(pages));
	}



	/**
	 * 新增 
	 */
	@PostMapping("/save")
	@ApiOperationSupport(order = 4)
	@ApiOperation(value = "新增", notes = "传入topic")
	public R save(@Valid @RequestBody Topic topic) {


		return R.status(topicService.save(topic));
	}

	/**
	 * 修改 
	 */
	@PostMapping("/update")
	@ApiOperationSupport(order = 5)
	@ApiOperation(value = "修改", notes = "传入topic")
	public R update(@Valid @RequestBody Topic topic) {
		return R.status(topicService.updateById(topic));
	}

	/**
	 * 新增或修改 
	 */
	@PostMapping("/submit")
	@ApiOperationSupport(order = 6)
	@ApiOperation(value = "新增或修改", notes = "传入topic")
	public R submit(@Valid @RequestBody Topic topic) {
		return R.status(topicService.saveOrUpdate(topic));
	}

	
	/**
	 * 删除 
	 */
	@PostMapping("/remove")
	@ApiOperationSupport(order = 8)
	@ApiOperation(value = "删除", notes = "传入ids")
	public R remove(@ApiParam(value = "主键集合", required = true) @RequestParam String ids) {
		return R.status(topicService.removeByIds(Func.toLongList(ids)));
	}



}
