package com.raysdata.rdc.dataasset.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum LabelRelationEnum {
	/**
	 * 资源目录
	 */
	catalog("catalog"),
	/**
	 * 数据库
	 */
	database("database"),
	/**
	 * 表
	 */
	table("table");

	final String name;
}
