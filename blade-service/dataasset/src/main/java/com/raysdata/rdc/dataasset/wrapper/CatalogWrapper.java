package com.raysdata.rdc.dataasset.wrapper;

import com.raysdata.rdc.dataasset.entity.Catalog;
import com.raysdata.rdc.dataasset.entity.Industry;
import com.raysdata.rdc.dataasset.entity.Topic;
import com.raysdata.rdc.dataasset.service.IIndustryService;
import com.raysdata.rdc.dataasset.service.ITopicService;
import com.raysdata.rdc.dataasset.utils.GetBeanUtil;
import com.raysdata.rdc.dataasset.vo.CatalogVO;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springblade.core.mp.support.BaseEntityWrapper;
import org.springblade.core.tool.api.R;
import org.springblade.core.tool.utils.BeanUtil;
import org.springblade.core.tool.utils.SpringUtil;
import org.springblade.system.entity.DictBiz;
import org.springblade.system.feign.IDictBizClient;
import org.springblade.system.user.entity.User;
import org.springblade.system.user.feign.IUserClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Optional;

@Component
@AllArgsConstructor
public class CatalogWrapper extends BaseEntityWrapper<Catalog, CatalogVO> {
	private static final Logger logger = LoggerFactory.getLogger(CatalogWrapper.class);
	@Autowired
	private  ITopicService topicService;
	@Autowired
	private  IIndustryService industryService;

	private final IDictBizClient dictBizClient;
	private final IUserClient userClient ;
	public static CatalogWrapper build() {
		/*userClient= SpringUtil.getBean(IUserClient.class);
		if (dictBizClient == null) {
			dictBizClient = SpringUtil.getBean(IDictBizClient.class);
		}*/
		return	GetBeanUtil.getBean(CatalogWrapper.class);
	}
	@Override
	public CatalogVO entityVO(Catalog o) {
		CatalogVO vo = Objects.requireNonNull(BeanUtil.copy(o, CatalogVO.class));
		Topic topic = topicService.getById(vo.getTopicId());
		Optional.ofNullable(topic).ifPresent(p ->vo.setTopicName(p.getName()));
		Industry industry= industryService.getById(vo.getIndustryId());
		Optional.ofNullable(industry).ifPresent(p ->vo.setIndustryName(p.getName()));
		R<String> dictBiz=dictBizClient.getValue("UPDATE_FREQUENCY",""+vo.getUpdateFrequencyDictId());
		logger.info(""+vo.getUpdateFrequencyDictId());
		logger.info("is null ? {}",dictBiz==null);
		if(null!=dictBiz){
			logger.info("obj is:{}",dictBiz);
		}
		Optional.ofNullable(dictBiz.getData()).ifPresent(p ->vo.setUpdateFrequencyDictName(p));
		R<String> dictBiz2=dictBizClient.getValue("SHARE_TYPE",""+vo.getShareTypeId());
		logger.info("obj is:{}",dictBiz2);
		Optional.ofNullable(dictBiz2.getData()).ifPresent(p ->vo.setShareTypeName(p));
		R<User> r=userClient.userInfoById(vo.getCreateUser());
		logger.info("{}",r);
		Optional.ofNullable(r.getData()).ifPresent(u ->vo.setCreateUserName(u.getAccount()));
		return vo;
	}
}
