/*
 *      Copyright (c) 2018-2028, Chill Zhuang All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the dreamlu.net developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: Chill 庄骞 (smallchill@163.com)
 */
package org.springblade.metadata.service.impl;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springblade.metadata.entity.MetadataVersion;
import org.springblade.metadata.vo.MetadataVersionVO;
import org.springblade.metadata.mapper.MetadataVersionMapper;
import org.springblade.metadata.service.IMetadataVersionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

/**
 *  服务实现类
 *
 * @author BladeX
 * @since 2021-05-26
 */
@Service
public class MetadataVersionServiceImpl extends ServiceImpl<MetadataVersionMapper, MetadataVersion> implements IMetadataVersionService {

	@Override
	public IPage<MetadataVersionVO> selectMetadataVersionPage(IPage<MetadataVersionVO> page, MetadataVersionVO metadataVersion) {
		return page.setRecords(baseMapper.selectMetadataVersionPage(page, metadataVersion));
	}

	@Override
	public void addMetadataVersion(MetadataVersion metadataVersion) {
		List<MetadataVersion> os = getByMetadataId(metadataVersion.getMetadataId());
		
		metadataVersion.setCreateTime(LocalDateTime.now());
		
		metadataVersion.setVersion(gerVersion(os));
		
		save(metadataVersion);
	}
    String gerVersion(List<MetadataVersion> os) {
    	if(os !=null  && !os.isEmpty()) {
    		List<MetadataVersion>  ns = os.stream().filter( o ->StringUtils.isNotEmpty(o.getVersion())).sorted(new Comparator<MetadataVersion>() {

				@Override
				public int compare(MetadataVersion o1, MetadataVersion o2) {
					Integer v1 =Integer.parseInt(o1.getVersion().replace("v", ""));
					Integer v2 =Integer.parseInt(o2.getVersion().replace("v", ""));
					if(v1 < v2) {
						return 1;
					} else  {
						return -1;
					}
				}
			}).collect(Collectors.toList());
    		return "v" + (Integer.parseInt(ns.get(0).getVersion().replace("v", ""))+1);
    		
    	}
    	return "v1";
    }
	@Override
	public List<MetadataVersion> getByMetadataId(String metadataId) {
		LambdaQueryWrapper<MetadataVersion>   w = Wrappers.lambdaQuery();
		w.eq(MetadataVersion::getMetadataId, metadataId);
		return baseMapper.selectList(w);
	}

	@Override
	public MetadataVersion latestMeta(MetadataVersion metadataVersion) {
		List<MetadataVersion> os = getByMetadataId(metadataVersion.getMetadataId());
		if(os !=null  && !os.isEmpty()) {
    		List<MetadataVersion>  ns = os.stream().filter( o ->StringUtils.isNotEmpty(o.getVersion())).sorted(new Comparator<MetadataVersion>() {

				@Override
				public int compare(MetadataVersion o1, MetadataVersion o2) {
					Integer v1 =Integer.parseInt(o1.getVersion().replace("v", ""));
					Integer v2 =Integer.parseInt(o2.getVersion().replace("v", ""));
					if(v1 < v2) {
						return 1;
					} else  {
						return -1;
					}
				}
			}).collect(Collectors.toList());
    		return  ns.get(0);
    		
    	}
		return null;
	}

}
