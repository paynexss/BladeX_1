package org.springblade.metadata.config;


import org.springblade.metadata.filter.MetaFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

//@Configuration
public class WebConfig {
	/**
	 * 注册第三方过滤器
	 * 功能与spring mvc中通过配置web.xml相同
	 * @return
	 */
	
//	@Bean
	public FilterRegistrationBean<MetaFilter> thirdFilter(@Autowired MetaFilter   metaFilter ){
		FilterRegistrationBean<MetaFilter> filterRegistrationBean = new FilterRegistrationBean<MetaFilter>() ;

		filterRegistrationBean.setFilter(metaFilter);
		List<String > urls = new ArrayList<>();
		// 匹配所有请求路径
		urls.add("/api/meta/api/atlas/**");
		filterRegistrationBean.setUrlPatterns(urls);

		return filterRegistrationBean;
	}

}
