package org.springblade.metadata.dto;

import java.util.List;

import org.apache.atlas.model.instance.AtlasEntity.AtlasEntityWithExtInfo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class EntityDTO {
	
	
	@ApiModelProperty(value = "category    id")
	private Long categoryId;
	
	@ApiModelProperty(value = "entity  info")
	private AtlasEntityWithExtInfo entity;
	
	@ApiModelProperty(value = "lables")
	private List<String> lables;
	
	@ApiModelProperty(value = "grade")
	private String grade;
}
