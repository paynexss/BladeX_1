package org.springblade.metadata.service;


import org.apache.atlas.AtlasClientV2;

import lombok.Data;


@Data
public class API_V2Url {
	private String url;
	private AtlasClientV2.API_V2 api;
	private String[]  ss;
	private String from;
}
