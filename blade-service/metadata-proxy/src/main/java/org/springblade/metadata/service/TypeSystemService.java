package org.springblade.metadata.service;

import org.apache.atlas.AtlasServiceException;
import org.apache.atlas.model.SearchFilter;
import org.apache.atlas.model.typedef.AtlasEntityDef;
import org.apache.atlas.model.typedef.AtlasTypesDef;

public interface TypeSystemService {
	public AtlasTypesDef  getAll(SearchFilter searchFilter) ;
	
	AtlasEntityDef getEntityByName(String name);
	AtlasEntityDef getEntityByGuid(String guid) throws AtlasServiceException;
	
	AtlasEntityDef  createEntityType(AtlasEntityDef  e)  throws AtlasServiceException;
	
	void removeEntityTypeByName(String name) throws AtlasServiceException;
	
	AtlasEntityDef update(AtlasEntityDef def)  throws AtlasServiceException;
	
	
}
