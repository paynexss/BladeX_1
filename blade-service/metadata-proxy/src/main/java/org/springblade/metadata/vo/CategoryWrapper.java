/*
 *      Copyright (c) 2018-2028, Chill Zhuang All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the dreamlu.net developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: Chill 庄骞 (smallchill@163.com)
 */
package org.springblade.metadata.vo;

import org.springblade.core.mp.support.BaseEntityWrapper;
import org.springblade.core.tool.constant.BladeConstant;
import org.springblade.core.tool.node.ForestNodeMerger;
import org.springblade.core.tool.utils.BeanUtil;
import org.springblade.core.tool.utils.Func;
import org.springblade.metadata.entity.Category;
import org.springblade.system.cache.DictCache;
import org.springblade.system.enums.DictEnum;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 包装类,返回视图层所需的字段
 *
 * @author Chill
 */
public class CategoryWrapper extends BaseEntityWrapper<Category, CategoryVO> {

	public static CategoryWrapper build() {
		return new CategoryWrapper();
	}

	@Override
	public CategoryVO entityVO(Category category) {
		CategoryVO categoryVO = Objects.requireNonNull(BeanUtil.copy(category, CategoryVO.class));
		if (Func.equals(category.getParentId(), BladeConstant.TOP_PARENT_ID)) {
			categoryVO.setParentName(BladeConstant.TOP_PARENT_NAME);
		} else {
//			Menu parent = SysCache.getMenu(menu.getParentId());
//			menuVO.setParentName(parent.getName());
		}
		String category1 = DictCache.getValue(DictEnum.MENU_CATEGORY, Func.toInt(categoryVO.getCategory()));
		String action = DictCache.getValue(DictEnum.BUTTON_FUNC, Func.toInt(categoryVO.getAction()));
		String open = DictCache.getValue(DictEnum.YES_NO, Func.toInt(categoryVO.getIsOpen()));
		categoryVO.setCategoryName(category1);
		categoryVO.setActionName(action);
		categoryVO.setIsOpenName(open);
		return categoryVO;
	}

	public List<CategoryVO> listNodeVO(List<Category> list) {
		List<CategoryVO> collect = list.stream().map(menu -> BeanUtil.copy(menu, CategoryVO.class)).collect(Collectors.toList());
		return ForestNodeMerger.merge(collect);
	}

	public List<CategoryVO> listNodeLazyVO(List<CategoryVO> list) {
		return ForestNodeMerger.merge(list);
	}

}
