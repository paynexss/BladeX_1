package org.springblade.metadata.entity;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;



@Data
public class UpgradeContent {
	private List<Content> attrs;
	
	@Data
	@AllArgsConstructor
	public static class Content {
		private Type type;
		private String from;
		private String to;
	}
	
	public enum Type{
		DELETE,MODIFY,ADD
	}
	
}
