package org.springblade.metadata.service;

import javax.ws.rs.core.MediaType;

import org.apache.atlas.AtlasClientV2;
import org.apache.atlas.AtlasServiceException;
import org.apache.commons.configuration.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class AtlasClientProxy extends AtlasClientV2 {
	private static final Logger LOG = LoggerFactory.getLogger(AtlasClientProxy.class);
	public AtlasClientProxy(Configuration configuration, String[] baseUrl, String[] basicAuthUserNamePassword) {
		super(configuration, baseUrl, basicAuthUserNamePassword);
		// TODO Auto-generated constructor stub
	}
	public AtlasClientProxy(String[] baseUrl, String[] basicAuthUserNamePassword) {
        super(baseUrl, basicAuthUserNamePassword);
    }
	
	public String callAPIWithResource(API api, WebResource resource, Object requestObject) throws AtlasServiceException {
	       return null;
	    }
}
