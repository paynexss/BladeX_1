package org.springblade.metadata.app;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;
import org.apache.atlas.AtlasBaseClient;
import org.apache.atlas.AtlasClientV2;
import org.apache.atlas.AtlasServiceException;
import org.apache.atlas.model.discovery.AtlasSearchResult;
import org.apache.atlas.model.discovery.SearchParameters;
import org.apache.atlas.model.instance.AtlasEntity;
import org.apache.atlas.model.instance.AtlasEntityHeader;
import org.apache.atlas.type.AtlasStructType;
import org.springblade.core.tool.api.R;
import org.springblade.metadata.service.API_V2Url;
import org.springblade.metadata.service.AtlasProxyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.HttpMethod;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("v2/metadata")
@Slf4j
public class MetadataController {

	private AtlasClientV2 atlasClientV2 = null;
	@Autowired
	private AtlasProxyService atlasProxyService;
	public static final String GETTABLES_URI = "api/atlas/v2/search/basic";
	public static final String GETCOLUMNS_URI = "api/atlas/v2/entity/guid/";

    @GetMapping("/getTables")
	public R<List<String>> getTables(@RequestParam Long datasourceId){
		API_V2Url au = atlasProxyService.getAPI(GETTABLES_URI, HttpMethod.POST);
		String[] ss = atlasProxyService.extract(au, GETTABLES_URI);
		String query = "datasourceid="+datasourceId;
		AtlasSearchResult atlasSearchResult = null;
		try {
			atlasSearchResult = atlasProxyService.callAPI(au.getApi(), query, ss);
		} catch (AtlasServiceException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		List<AtlasEntityHeader> atlasEntityHeaders = atlasSearchResult.getEntities();
		List<String> resultList = new ArrayList<>();
		for(AtlasEntityHeader atlasEntityHeader:atlasEntityHeaders){
			resultList.add(atlasEntityHeader.getAttribute("name").toString());
		}
		return R.data(resultList);
    }

	@GetMapping("/getColumns")
	public R<Object> getColumns(@RequestParam String tableId){
		API_V2Url au = atlasProxyService.getAPI(GETCOLUMNS_URI+tableId, HttpMethod.GET);
		String[] ss = atlasProxyService.extract(au, GETCOLUMNS_URI+tableId);
		SearchParameters query = new SearchParameters();
		//params.setQuery("datasourceid="+datasourceId);
		AtlasEntity atlasEntity = null;
		try {
			atlasEntity = atlasProxyService.callAPI(tableId);
		} catch (AtlasServiceException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		Map<String, Object> relationshipAttributes = atlasEntity.getRelationshipAttributes();
		Object list = relationshipAttributes.get("columns");
		//List<String> resultList = new ArrayList<>();
		/*for(AtlasEntityHeader atlasEntityHeader:atlasEntityHeaders){
			resultList.add(atlasEntityHeader.);
		}*/
		return R.data(list);
	}



    AtlasBaseClient.API formatPathParameters(AtlasBaseClient.API api, String... params) {
		return new AtlasBaseClient.API(String.format(api.getPath(), params), api.getMethod(), api.getExpectedStatus());
	}
}
