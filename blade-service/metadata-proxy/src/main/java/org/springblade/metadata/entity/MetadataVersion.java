/*
 *      Copyright (c) 2018-2028, Chill Zhuang All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the dreamlu.net developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: Chill 庄骞 (smallchill@163.com)
 */
package org.springblade.metadata.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 实体类
 *
 * @author BladeX
 * @since 2021-05-26
 */
@Data
@TableName("rays_metadata_version")
@ApiModel(value = "MetadataVersion对象", description = "MetadataVersion对象")
public class MetadataVersion implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	* 主键
	*/
		@ApiModelProperty(value = "主键")
		private Long id;
	/**
	* 表id
	*/
		@ApiModelProperty(value = "表id")
		private String metadataId;
	/**
	* 版本
	*/
		@ApiModelProperty(value = "版本")
		private String version;
	/**
	* 数据源id
	*/
		@ApiModelProperty(value = "数据源id")
		private String contentJson;
	/**
	* 数据源名称
	*/
		@ApiModelProperty(value = "数据源名称")
		private String publisher;
	/**
	* 创建时间
	*/
		@ApiModelProperty(value = "创建时间")
		private LocalDateTime createTime;
		
		@ApiModelProperty(value = "upgrade content")
       private String upJson;
}
