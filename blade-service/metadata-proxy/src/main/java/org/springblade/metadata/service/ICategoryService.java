/*
 *      Copyright (c) 2018-2028, Chill Zhuang All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the dreamlu.net developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: Chill 庄骞 (smallchill@163.com)
 */
package org.springblade.metadata.service;

import java.util.List;
import java.util.Map;

import org.springblade.core.secure.BladeUser;
import org.springblade.core.tool.support.Kv;
import org.springblade.metadata.entity.Category;
import org.springblade.metadata.vo.CategoryVO;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 服务类
 *
 * @author Chill
 */
public interface ICategoryService extends IService<Category> {

	/**
	 * 懒加载列表
	 *
	 * @param parentId
	 * @param param
	 * @return
	 */
	List<CategoryVO> lazyList(Long parentId, Map<String, Object> param);
    
	/**
	 * 懒加载列表
	 *
	 * @param parentId
	 * @param typeCategoty
	 * @return
	 */
	List<CategoryVO> availableCategory(Long parentId,String typeCategoty);

	/**
	 * 菜单树形结构
	 *
	 * @param roleId
	 * @param topMenuId
	 * @return
	 */
	List<CategoryVO> routes(String roleId, Long topMenuId,String category);

	/**
	 * 按钮树形结构
	 *
	 * @param roleId
	 * @return
	 */
	List<CategoryVO> buttons(String roleId);

	/**
	 * 树形结构
	 *
	 * @return
	 */
	List<CategoryVO> tree();

	/**
	 * 授权树形结构
	 *
	 * @param user
	 * @return
	 */
	List<CategoryVO> grantTree(BladeUser user);

	/**
	 * 顶部菜单树形结构
	 *
	 * @param user
	 * @return
	 */
	List<CategoryVO> grantTopTree(BladeUser user);

	/**
	 * 数据权限授权树形结构
	 *
	 * @param user
	 * @return
	 */
	List<CategoryVO> grantDataScopeTree(BladeUser user);

	/**
	 * 接口权限授权树形结构
	 *
	 * @param user
	 * @return
	 */
	List<CategoryVO> grantApiScopeTree(BladeUser user);

	/**
	 * 默认选中节点
	 *
	 * @param roleIds
	 * @return
	 */
	List<String> roleTreeKeys(String roleIds);

	/**
	 * 默认选中节点
	 *
	 * @param topMenuIds
	 * @return
	 */
	List<String> topTreeKeys(String topMenuIds);

	/**
	 * 默认选中节点
	 *
	 * @param roleIds
	 * @return
	 */
	List<String> dataScopeTreeKeys(String roleIds);

	/**
	 * 默认选中节点
	 *
	 * @param roleIds
	 * @return
	 */
	List<String> apiScopeTreeKeys(String roleIds);

	/**
	 * 获取配置的角色权限
	 *
	 * @param user
	 * @return
	 */
	List<Kv> authRoutes(BladeUser user);


	/**
	 * 提交
	 *
	 * @param menu
	 * @return
	 */
	boolean submit(Category menu);

	boolean removeCategory(String ids);

	List<CategoryVO> lazyCategoryList(Long parentId, Map<String, Object> param);
	
	Category getBydCode(String code);
	
	Category getBydOutCode(String code);

}
