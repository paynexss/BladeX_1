package org.springblade.metadata.app;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/test")
@Slf4j
public class IndexController {
    @RequestMapping(value = "*",method=RequestMethod.GET)
    String index(HttpServletRequest request){
    	String path  = request.getContextPath();
    	log.info(path);
        return "index";
    }
}