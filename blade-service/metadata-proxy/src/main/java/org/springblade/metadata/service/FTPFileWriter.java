package org.springblade.metadata.service;

import java.io.InputStream;

public interface FTPFileWriter {


    /**
     * Store a file on the ftp server.
     *
     * @param inputStream Stream the new file is read from.
     * @param destPath    Remote path the file should be placed at.
     * @param append      Append to an existing file or write as a new file.
     * @return boolean True if successful, False otherwise.
     */

	String saveFile(InputStream inputStream, String destPath, boolean append);

    /**
     * Store a file on the ftp server.
     *
     * @param sourcePath Local path the file is read from.
     * @param destPath   Remote path the file should be placed at.
     * @param append      Append to an existing file or write as a new file.
     * @return boolean True if successful, False otherwise.
     */

	String saveFile(String sourcePath, String destPath, boolean append);
}
