package org.springblade.metadata;

import org.springblade.core.cloud.feign.EnableBladeFeign;
import org.springblade.core.launch.BladeApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cloud.client.SpringCloudApplication;

@EnableBladeFeign
@SpringCloudApplication
@ServletComponentScan
public class MetadataProxyApplication {

	public static void main(String[] args) {
		BladeApplication.run("metadata-proxy", MetadataProxyApplication.class, args);


	}

}
