/*
 *      Copyright (c) 2018-2028, Chill Zhuang All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the dreamlu.net developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: Chill 庄骞 (smallchill@163.com)
 */
package org.springblade.metadata.service.impl;

import static org.springblade.core.cache.constant.CacheConstant.MENU_CACHE;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springblade.core.log.exception.ServiceException;
import org.springblade.core.secure.BladeUser;
import org.springblade.core.secure.utils.AuthUtil;
import org.springblade.core.tool.constant.BladeConstant;
import org.springblade.core.tool.node.ForestNodeMerger;
import org.springblade.core.tool.support.Kv;
import org.springblade.core.tool.utils.Func;
import org.springblade.core.tool.utils.StringUtil;
import org.springblade.metadata.dto.CategoryDTO;
import org.springblade.metadata.entity.Category;
import org.springblade.metadata.mapper.CategoryMapper;
import org.springblade.metadata.service.ICategoryService;
import org.springblade.metadata.vo.CategoryVO;
import org.springblade.metadata.vo.CategoryWrapper;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import lombok.AllArgsConstructor;

/**
 * 服务实现类
 *
 * @author Chill
 */
@Service
@AllArgsConstructor
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements ICategoryService {

	private final static String PARENT_ID = "parentId";
	private final static Integer MENU_CATEGORY = 1;

	@Override
	public List<CategoryVO> lazyList(Long parentId, Map<String, Object> param) {
		if (Func.isEmpty(Func.toStr(param.get(PARENT_ID)))) {
			parentId = null;
		}
		return baseMapper.lazyList(parentId, param);
	}

	@Override
	public List<CategoryVO> lazyCategoryList(Long parentId, Map<String, Object> param) {
		if (Func.isEmpty(Func.toStr(param.get(PARENT_ID)))) {
			parentId = null;
		}
		return baseMapper.lazyCategoryList(parentId, param);
	}


	@Override
	public List<CategoryVO> routes(String roleId, Long topCategoryId,String category) {
		if (StringUtil.isBlank(roleId)) {
			return null;
		}
		List<Category> allCategorys = baseMapper.allCategory( category);
		List<Category> roleCategorys = (AuthUtil.isAdministrator() && Func.isEmpty(topCategoryId)) ? allCategorys : baseMapper.roleCategory(Func.toLongList(roleId), topCategoryId);
		return buildRoutes(allCategorys, allCategorys);
	}

	private List<CategoryVO> buildRoutes(List<Category> allCategorys, List<Category> roleCategorys) {
		List<Category> routes = new LinkedList<>(roleCategorys);
		roleCategorys.forEach(roleCategory -> recursion(allCategorys, routes, roleCategory));
		routes.sort( new Comparator<Category>() {

			@Override
			public int compare(Category o1, Category o2) {
				if(o1.getSort()!=null && o2.getSort()!=null) {
					if(o1.getSort()>o2.getSort()) {
						return 1;
					}else {
						return -1;
					}
				}
				return 0;
			}
		});
		CategoryWrapper menuWrapper = new CategoryWrapper();
//		List<Category> collect = routes.stream().filter(x -> Func.equals(x.getCategory(), 0)).collect(Collectors.toList());
		return menuWrapper.listNodeVO(routes);
	}

	private void recursion(List<Category> allCategorys, List<Category> routes, Category roleCategory) {
		Optional<Category> menu = allCategorys.stream().filter(x -> Func.equals(x.getId(), roleCategory.getParentId())).findFirst();
		if (menu.isPresent() && !routes.contains(menu.get())) {
			routes.add(menu.get());
			recursion(allCategorys, routes, menu.get());
		}
	}

	@Override
	public List<CategoryVO> buttons(String roleId) {
		List<Category> buttons = (AuthUtil.isAdministrator()) ? baseMapper.allButtons() : baseMapper.buttons(Func.toLongList(roleId));
		CategoryWrapper menuWrapper = new CategoryWrapper();
		return menuWrapper.listNodeVO(buttons);
	}

	@Override
	public List<CategoryVO> tree() {
		return ForestNodeMerger.merge(baseMapper.tree());
	}

	@Override
	public List<CategoryVO> grantTree(BladeUser user) {
		return ForestNodeMerger.merge(user.getTenantId().equals(BladeConstant.ADMIN_TENANT_ID) ? baseMapper.grantTree() : baseMapper.grantTreeByRole(Func.toLongList(user.getRoleId())));
	}

	@Override
	public List<CategoryVO> grantTopTree(BladeUser user) {
		return ForestNodeMerger.merge(user.getTenantId().equals(BladeConstant.ADMIN_TENANT_ID) ? baseMapper.grantTopTree() : baseMapper.grantTopTreeByRole(Func.toLongList(user.getRoleId())));
	}

	@Override
	public List<CategoryVO> grantDataScopeTree(BladeUser user) {
		return ForestNodeMerger.merge(user.getTenantId().equals(BladeConstant.ADMIN_TENANT_ID) ? baseMapper.grantDataScopeTree() : baseMapper.grantDataScopeTreeByRole(Func.toLongList(user.getRoleId())));
	}

	@Override
	public List<CategoryVO> grantApiScopeTree(BladeUser user) {
		return ForestNodeMerger.merge(user.getTenantId().equals(BladeConstant.ADMIN_TENANT_ID) ? baseMapper.grantApiScopeTree() : baseMapper.grantApiScopeTreeByRole(Func.toLongList(user.getRoleId())));
	}

	@Override
	public List<String> roleTreeKeys(String roleIds) {
		return null;
	}

	@Override
	public List<String> topTreeKeys(String topCategoryIds) {
		return null;
	}

	@Override
	public List<String> dataScopeTreeKeys(String roleIds) {
		return null;
	}

	@Override
	public List<String> apiScopeTreeKeys(String roleIds) {
		return null;
	}

	@Override
	@Cacheable(cacheNames = MENU_CACHE, key = "'auth:routes:' + #user.roleId")
	public List<Kv> authRoutes(BladeUser user) {
		List<CategoryDTO> routes = baseMapper.authRoutes(Func.toLongList(user.getRoleId()));
		List<Kv> list = new ArrayList<>();
		routes.forEach(route -> list.add(Kv.create().set(route.getPath(), Kv.create().set("authority", Func.toStrArray(route.getAlias())))));
		return list;
	}

	@Override
	public boolean removeCategory(String ids) {
		Integer cnt = baseMapper.selectCount(Wrappers.<Category>query().lambda().in(Category::getParentId, Func.toLongList(ids)));
		if (cnt > 0) {
			throw new ServiceException("请先删除子节点!");
		}
		return removeByIds(Func.toLongList(ids));
	}

	@Override
	public boolean submit(Category menu) {
		LambdaQueryWrapper<Category> menuQueryWrapper = Wrappers.lambdaQuery();
		if (menu.getId() == null) {
			menuQueryWrapper.eq(Category::getCode, menu.getCode()).or(
				wrapper -> wrapper.eq(Category::getName, menu.getName()).eq(Category::getCategory, MENU_CATEGORY)
			);
		} else {
			menuQueryWrapper.ne(Category::getId, menu.getId()).and(
				wrapper -> wrapper.eq(Category::getCode, menu.getCode()).or(
					o -> o.eq(Category::getName, menu.getName()).eq(Category::getCategory, MENU_CATEGORY)
				)
			);
		}
		Integer cnt = baseMapper.selectCount(menuQueryWrapper);
		if (cnt > 0) {
			throw new ServiceException("菜单名或编号已存在!");
		}
		if (menu.getParentId() == null && menu.getId() == null) {
			menu.setParentId(BladeConstant.TOP_PARENT_ID);
		}
		menu.setIsDeleted(BladeConstant.DB_NOT_DELETED);
		return saveOrUpdate(menu);
	}
   
	public Category getBydCode(String code) {
		LambdaQueryWrapper<Category> menuQueryWrapper = Wrappers.lambdaQuery();
		menuQueryWrapper.eq(Category::getCode, code);
		List<Category> cs = baseMapper.selectList(menuQueryWrapper);
		if(cs ==null || cs.isEmpty()) {
			return null;
		}
		return  cs.get(0);
	}

	@Override
	public Category getBydOutCode(String code) {
		LambdaQueryWrapper<Category> menuQueryWrapper = Wrappers.lambdaQuery();
		menuQueryWrapper.eq(Category::getOutCode, code);
		List<Category> cs = baseMapper.selectList(menuQueryWrapper);
		if(cs ==null || cs.isEmpty()) {
			return null;
		}
		return  cs.get(0);
	}

	@Override
	public List<CategoryVO> availableCategory(Long parentId, String typeCategoty) {
		// TODO Auto-generated method stub
		return baseMapper.availableCategory(parentId, typeCategoty);
	}

}
