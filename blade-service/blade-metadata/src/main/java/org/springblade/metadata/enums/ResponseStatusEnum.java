package org.springblade.metadata.enums;



/**
 *
 *
 * @author syq
 * @email 442365450@qq.com
 * @date 2018-08-08 17:40:38
 */
public enum ResponseStatusEnum {
	/** * 操作成功 */
	RESCODE_0(0, "操作成功"),
	RESCODE_00(0, "指令下发成功"),
	/** *服务器发生异常 */
	RESCODE_1(1, "服务器发生异常"),
	RESCODE_2(2, "未登录"),
	/** *操作失败! */
	RESCODE_3(3, "无权限"),
	RESCODE_4(4, "非法TOKEN"),
	RESCODE_5(5, "非法参数"),
	RESCODE_6(6, "rundeck操作失败"),
	RESCODE_7(7, "持久化失败"),
	RESCODE_8(8, "密码已存在"),
	RESCODE_9(9, "密钥已经存在"),
	RESCODE_10(10, "未获取到HUB TOKEN"),


	RESCODE_101(101, "数据记录不存在"),
	RESCODE_102(102, "数据记录状态不正确"),

	RESCODE_11(11,"未获取到租户信息"),
	RESCODE_12(12, "创建资源池db失败"),

	RESCODE_15(15, "重命名资源池db失败"),
	RESCODE_17(17, "删除资源池db失败"),
	RESCODE_19(19, "删除资源池失败：资源池中有物理机，不允许删除"),
	RESCODE_31(31, "重启虚拟机失败：当前状态不支持重启"),

	RESCODE_32(32, "CPU配额不足"),
	RESCODE_33(33, "内存配额不足"),
	RESCODE_37(37, "存储配额不足"),
	RESCODE_34(34, "请先关闭虚拟机[%s]"),
	RESCODE_35(35, "存储创建失败 (vmware) 请选择目标虚机"),
	RESCODE_36(36, "请选择创建时的租户[%s]进行变更操作"),
    RESCODE_38(38, "当前磁盘已经挂载在虚拟机[%s]上，不能再次挂载"),
    RESCODE_39(39, "变更失败：请确认当前虚拟机状态为[已停止]状态"),
	RESCODE_40(40, "磁盘状态不可删除 请先卸载"),
	RESCODE_41(41, "虚拟机名字重复，不能修改，请核查"),
	RESCODE_42(42, "未找到虚拟机，请核查"),
	RESCODE_43(43, "修改虚拟机信息失败"),
	RESCODE_44(44, "IP非法 [%s]"),
	RESCODE_45(45, "添加网络失败"),
	RESCODE_46(46, "[%s]可分配IP不足"),
	RESCODE_47(47, "添加网络失败：请确认当前虚拟机状态为[已开机]状态"),
	RESCODE_48(48, "未获取到新增网络的mac地址"),
	RESCODE_49(49, "未找到网卡，请核查"),
	RESCODE_50(50, "删除网络失败"),
	RESCODE_51(51, "未找到宿主机，请核查"),
	RESCODE_52(52, "未找到存储，请核查"),
	RESCODE_53(53, "当前网络是虚拟机默认的网络，不允许删除"),
	RESCODE_54(54, "虚拟机迁移失败"),
	RESCODE_55(55, "未找到虚拟磁盘，请核查"),
	RESCODE_56(56, "磁盘状态不可扩容 请先挂载"),
	RESCODE_57(57, "磁盘仅支持扩容"),
	RESCODE_58(58, "磁盘扩容失败"),
	RESCODE_59(59, "请填写1-50正整数"),
	RESCODE_60(60, "开始实践和结束时间不能为空，并且开始时间要小于结束时间"),



	RESCODE_1001(1001,"删除任务失败"),
	RESCODE_1002(1002,"未找到相关的任务定义详情"),
	RESCODE_1003(1003,"任务运行失败"),

	RESCODE_2001(2001,"未找到相关表单详情"),
	RESCODE_2002(2002,"相关表单已失效或已删除"),
	RESCODE_2003(2003,"未找到相关表单模型详情"),

	RESCODE_3003(3003,"kafka topic 创建失败"),
	RESCODE_20000(20000, "非法Authentication"),
	RESCODE_100_0001(1000001,"token为空或非法的token"),
	RESCODE_100_1002(1001002,"token已禁用或过期"),
	;


	/*SUCCESS(0),//成功
    UNKNOWERROR(0),//未知错误
    
    UNLOGIN(2),//未登录
    UNAUTHORITY(3),//无权限
    ILLEGALTOKEN(4),//非法TOKEN
	ILLEGALPOST(5),//非法POST参数
	
	RUNDECKERROR(6),//rundeck操作失败
	STORAGEFAILURE(7),//持久化失败
	
	PASSWORDALREADYEXIST(8),
	KEYALREADYEXIST(9);*/

	/**
	 * 枚举的值
	 * */
	private long code;

	/**
	 * 枚举的中文描述
	 * */
	private String desc;


	private ResponseStatusEnum(int code, String desc) {
		this.code = code;
		this.desc = desc;
	}


	public static String getDesc(long code) {
		for (ResponseStatusEnum b : ResponseStatusEnum.values()) {
			if (b.code == code) {
				return b.desc;
			}
		}
		return "";
	}


	/**
	 * 匹配提示码 未匹配的返回错误码1
	 * @param code
	 * @return
	 */
	public static ResponseStatusEnum getResponseStatusEnum(long code) {
		for (ResponseStatusEnum b : ResponseStatusEnum.values()) {
			if (b.code == code) {
				return b;
			}
		}
		return ResponseStatusEnum.RESCODE_1;
	}


	public long getCode() {
		return code;
	}


	public void setCode(long code) {
		this.code = code;
	}


	public String getDesc() {
		return desc;
	}


	public void setDesc(String desc) {
		this.desc = desc;
	}
}
