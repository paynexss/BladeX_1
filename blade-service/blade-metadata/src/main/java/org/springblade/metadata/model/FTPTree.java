package org.springblade.metadata.model;

import java.util.ArrayList;
import java.util.List;

public class FTPTree {
	private String name;

	private List<FTPTree> children;

	public FTPTree() {
	}

	public FTPTree(String name) {
		this.name = name;
		this.children = new ArrayList<>();
	}

	public FTPTree(String name, List<FTPTree> children) {
		this.name = name;
		this.children = children;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<FTPTree> getChildren() {
		return children;
	}

	public void setChildren(List<FTPTree> children) {
		this.children = children;
	}
}
