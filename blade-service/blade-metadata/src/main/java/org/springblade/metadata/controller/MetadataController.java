/*
 *      Copyright (c) 2018-2028, Chill Zhuang All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the dreamlu.net developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: Chill 庄骞 (smallchill@163.com)
 */
package org.springblade.metadata.controller;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.raysdata.atlas.mysql.feign.IMysqlClient;
import com.raysdata.atlas.oracle.feign.IOracleClient;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.springblade.core.boot.ctrl.BladeController;
import org.springblade.core.tenant.annotation.NonDS;
import org.springblade.core.tool.api.R;
import org.springblade.metadata.enums.DbType;
import org.springblade.metadata.model.DataSource;
import org.springblade.metadata.model.FTPTree;
import org.springblade.metadata.service.FTPFileWriter;
import org.springblade.metadata.service.MetadataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

/**
 * 顶部菜单表 控制器
 *
 * @author BladeX
 */
@NonDS
@RestController
@AllArgsConstructor
@RequestMapping("/metadata")
@Api(value = "查询表", tags = "查询表")
public class MetadataController extends BladeController {

	private final IMysqlClient iMysqlClient;
	private final IOracleClient iOracleClient;
	@Autowired
	private MetadataService metadataService;

	@Autowired
	private FTPFileWriter ftpFileWriter;
	/**
	 * 查询表
	 */
	@GetMapping("/tables")
	@ApiOperationSupport(order = 1)
	@ApiOperation(value = "表", notes = "传入datasourceId")
	public R<List<String>> detail(@RequestParam String datasourceId) {
		DataSource dataSource = metadataService.getDataSource(datasourceId);
		if(DbType.MYSQL.equals(dataSource.getType())){
			return iMysqlClient.getTables(datasourceId);
		}else if (DbType.ORACLE.equals(dataSource.getType())){
			return iOracleClient.getTables(datasourceId);
		}else {
			return null;
		}

	}

	/**
	 * 查询表字段
	 */
	@GetMapping("/columns")
	@ApiOperationSupport(order = 2)
	@ApiOperation(value = "列", notes = "传入datasourceId,tableName")
	public R<List<String>> list(@RequestParam String datasourceId,String tableName) {
		DataSource dataSource = metadataService.getDataSource(datasourceId);
		if(DbType.MYSQL.equals(dataSource.getType())){
			return iMysqlClient.getColumns(datasourceId,tableName);
		}else if (DbType.ORACLE.equals(dataSource.getType())){
			return iOracleClient.getColumns(datasourceId,tableName);
		}else {
			return null;
		}
	}


	/**
	 * 列出FTP目录下的目录
	 */
	@GetMapping("/ftp/directories")
	@ApiOperationSupport(order = 1)
	@ApiOperation(value = "目录", notes = "传入datasourceId")
	public R<FTPFile[]> listDirectories(@RequestParam String datasourceId,String directory) {
		DataSource dataSource = metadataService.getDataSource(datasourceId);
        return R.data(ftpFileWriter.listDirectories(dataSource,directory));
	}

	/**
	 * 列出FTP目录下的文件
	 */
	@GetMapping("/ftp/files")
	@ApiOperationSupport(order = 1)
	@ApiOperation(value = "目录", notes = "传入datasourceId")
	public R<FTPFile[]> listFiles(@RequestParam String datasourceId,String directory) {
		DataSource dataSource = metadataService.getDataSource(datasourceId);
		return R.data(ftpFileWriter.listFiles(dataSource,directory));
	}

	@ApiOperation("递归列出FTP目录下的内容（包括文件和目录）")
	@GetMapping("/ftp/recursionDirFile")
	public R<FTPTree> recursionDirFile(@RequestParam String datasourceId, String directory) throws Exception {
		DataSource dataSource = metadataService.getDataSource(datasourceId);
		return R.data(ftpFileWriter.recursionDirFile(dataSource,directory));
	}

	@ApiOperation("递归列出FTP目录下的内容（只包括目录）")
	@GetMapping("/ftp/recursionDir")
	public R<FTPTree> recursionDir(@RequestParam String datasourceId, String directory) throws Exception {
		DataSource dataSource = metadataService.getDataSource(datasourceId);
		return R.data(ftpFileWriter.recursionDir(dataSource,directory));
	}




}
