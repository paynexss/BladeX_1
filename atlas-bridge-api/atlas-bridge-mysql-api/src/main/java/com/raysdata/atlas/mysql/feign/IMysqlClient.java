/*
 *      Copyright (c) 2018-2028, Chill Zhuang All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the dreamlu.net developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: Chill 庄骞 (smallchill@163.com)
 */
package com.raysdata.atlas.mysql.feign;


import org.springblade.core.tool.api.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Feign接口类
 *
 * @author Chill
 */
@FeignClient(
	value = "mysqlBridge"
)
public interface IMysqlClient {

	String API_PREFIX = "/client";
	String GET_TABELS = API_PREFIX + "/get-tabels";
	String GET_COLUMNS = API_PREFIX + "/get-columns";

	/**
	 * 获取表
	 *
	 * @param dataSourceId 源编号
	 * @return
	 */
	@GetMapping(GET_TABELS)
	R<List<String>> getTables(@RequestParam("dataSourceId") String dataSourceId);

	/**
	 * 获取表
	 *
	 * @param dataSourceId 源编号
	 * @return
	 */
	@GetMapping(GET_COLUMNS)
	R<List<String>> getColumns(@RequestParam("dataSourceId") String dataSourceId,@RequestParam("tableName") String tableName);

}
