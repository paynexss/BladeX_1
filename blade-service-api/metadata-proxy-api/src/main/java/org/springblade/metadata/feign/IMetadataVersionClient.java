/*
 *      Copyright (c) 2018-2028, Chill Zhuang All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the dreamlu.net developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: Chill 庄骞 (smallchill@163.com)
 */
package org.springblade.metadata.feign;

import org.springblade.core.tool.api.R;
import org.springblade.metadata.entity.MetadataVersion;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * Notice Feign接口类
 *
 * @author Chill
 */
@FeignClient(
	value = "metadata-proxy",path = "/api/meta"
)
public interface IMetadataVersionClient {

	String API_PREFIX = "/client";
	String SAVE_METADATA_VERSION = API_PREFIX + "/save-metadata-version";

	/**
	 * 新建元数据版本
	 *
	 * @param metadataVersion 元数据版本实体
	 * @return
	 */
	@PostMapping(SAVE_METADATA_VERSION)
	void saveMetadataVersion(@RequestBody MetadataVersion metadataVersion);
}
